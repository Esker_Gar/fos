﻿using System.Collections;
using UnityEngine;

namespace Code.Game
{
	public interface ICoroutines
	{
		Coroutine StartCoroutine(IEnumerator routine);
		void StopCoroutine(Coroutine routine);
	}
	
	public class Coroutines : MonoBehaviour, ICoroutines
	{
	}
}