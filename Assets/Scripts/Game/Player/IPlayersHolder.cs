﻿using System.Collections.Generic;

namespace Code.Game
{
	public interface IPlayersHolder
	{
		void Init();
		Player GetLocalPlayer();
		IEnumerable<Player> GetAllPlayers();
	}
}