﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Code.Game
{
	public enum Direction
	{
		DownRight = 1,
		UpLeft = -1
	}

	public class Player
	{
		public bool IsPlayerTurn {get; set;}
		public bool IsFirstTurn {get; set;} = true;
		public int Score {get; private set;}
		public Color Color {get;}
		public Direction Direction {get;}
		public Cell InitialCell {get;}
		public HashSet<Cell> Cells {get;}
		[CanBeNull] public Cell PreviousFigureInitCell {get; set;}
		public Dictionary<int, int> HorizontalInitialIndex => _horizontalInitialIndex;

		private readonly Dictionary<int, int> _horizontalInitialIndex;
		private Vector2 _maxValue;
		private Vector2 _minValue;

		public Player(Cell initialCell, Color color, Direction direction)
		{
			Cells = new HashSet<Cell>();
			InitialCell = initialCell;
			_horizontalInitialIndex = new Dictionary<int, int>
			{
				{InitialCell.VerticalIndex, InitialCell.HorizontalIndex}
			};
			Color = color;
			Direction = direction;
			_minValue = new Vector2(short.MaxValue, short.MaxValue);
		}
		
		public Vector2 GetCellValue()
		{
			return Direction == Direction.DownRight ? _maxValue : _minValue;
		}

		public void AddCell(Cell cell)
		{
			Cells.Add(cell);
			Score++;
		}

		public void AddHorizontalIndex(int index, int maxValue)
		{
			if (_horizontalInitialIndex.ContainsKey(index) == false)
			{
				_horizontalInitialIndex.Add(index, maxValue);
			}
			else
			{
				var conditionUtils = new ConditionUtils(Direction);
				var horizontalInitialIndex = _horizontalInitialIndex[index];

				if (conditionUtils.CheckStrictCondition(horizontalInitialIndex, maxValue))
				{
					_horizontalInitialIndex[index] = maxValue;
				}
			}
		}
		
		public void RefreshPlayerCellInfo(GridFigure gridFigure)
		{
			var maxValue = gridFigure.GetMaxCellsValues();
			var minValue = gridFigure.GetMinCellsValues();
			
			_maxValue = GetRefreshedValue(_maxValue, maxValue);
			_minValue = GetRefreshedValue(_minValue, minValue);
			
			if (_maxValue.x < maxValue.x)
			{
				_maxValue = new Vector2(maxValue.x, _maxValue.y);
			}
			
			if (_maxValue.y < maxValue.y)
			{
				_maxValue = new Vector2(_maxValue.x, maxValue.y);
			}
			
			if (_minValue.x > minValue.x)
			{
				_minValue = new Vector2(minValue.x, _minValue.y);
			}
			
			if (_minValue.y > minValue.y)
			{
				_minValue = new Vector2(_minValue.x, minValue.y);
			}
		}

		private Vector2 GetRefreshedValue(Vector2 playerValue, Vector2 figureValue)
		{
			var conditionUtils = new ConditionUtils(Direction);
			
			if (conditionUtils.CheckStrictCondition((int)playerValue.x ,(int)figureValue.x))
			{
				playerValue = new Vector2(figureValue.x, playerValue.y);
			}
			
			if (conditionUtils.CheckStrictCondition((int)playerValue.y ,(int)figureValue.y))
			{
				playerValue = new Vector2(playerValue.x, figureValue.y);
			}

			return playerValue;
		}
	}
}