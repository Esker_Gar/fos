﻿using System;
using Code.Framework;
using UnityEngine;

namespace Code.Game
{
    public class CurrentPlayerChanger : IInitializable
    {
        [Inject] private readonly IPlayersHolder _playersHolder;

        public Player LocalPlayer => _playersHolder.GetLocalPlayer();
        public Player CurrentPlayer {get; private set;}
        public Color CurrentPlayerColor {get; set;}
        public event Action OnPlayerChanged;
        
        public void Init()
        {
            CurrentPlayer = LocalPlayer;
        }

        public void ChangePlayer()
        {
            var players = _playersHolder.GetAllPlayers();

            foreach (var player in players)
            {
                player.IsPlayerTurn = !player.IsPlayerTurn;

                if (player.IsPlayerTurn)
                {
                    CurrentPlayer = player;
                }
            }

            OnPlayerChanged?.Invoke();
        }
    }
}