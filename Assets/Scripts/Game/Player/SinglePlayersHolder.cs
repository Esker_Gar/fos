﻿using System.Collections.Generic;
using Code.Framework;
using Code.Static;
using UnityEngine;

namespace Code.Game
{
	public class SinglePlayersHolder : IPlayersHolder
	{
		[Inject] private readonly GridModel _gridModel;
		[Inject] private readonly Configs _configs;

		private Player _localPlayer;
		private Player _aiPlayer;

		public void Init()
		{
			_localPlayer = new Player(_gridModel.LeftSideInitCell, _configs.ConfigColors.FirstPlayerCellsColor, Direction.DownRight);
			_aiPlayer =	new Player(_gridModel.RightSideInitCell, _configs.ConfigColors.SecondPlayerCellsColor, Direction.UpLeft);
			_localPlayer.IsPlayerTurn = true;
		}
		
		public Player GetLocalPlayer()
		{
			return _localPlayer;
		}

		public IEnumerable<Player> GetAllPlayers()
		{
			var players = new Player[2];
			
			players[0] = _localPlayer;
			players[1] = _aiPlayer;

			return players;
		}
	}
}