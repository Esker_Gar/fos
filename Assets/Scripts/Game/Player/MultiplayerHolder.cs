﻿using System.Collections.Generic;
using Code.Framework;
using Code.Static;
using Photon.Pun;

namespace Code.Game
{
    public class MultiplayerHolder : IPlayersHolder
    {
        [Inject] private readonly GridModel _gridModel;
        [Inject] private readonly Configs _configs;

        private Player _localPlayer;
        
        public void Init()
        {
            _localPlayer = PhotonNetwork.IsMasterClient ?
                new Player(_gridModel.LeftSideInitCell, _configs.ConfigColors.FirstPlayerCellsColor, Direction.DownRight) :
                new Player(_gridModel.RightSideInitCell, _configs.ConfigColors.SecondPlayerCellsColor, Direction.UpLeft);
            _localPlayer.IsPlayerTurn = PhotonNetwork.IsMasterClient;
        }
        
        public Player GetLocalPlayer()
        {
            return _localPlayer;
        }

        public IEnumerable<Player> GetAllPlayers()
        {
            var players = new Player[1];
            
            players[0] = _localPlayer;

            return players;
        }
    }
}