﻿using Code.Game;
using UnityEngine.SceneManagement;

namespace Code.Framework
{
	public class MainInitContext : InitContext
	{
		[Inject] private readonly AdsManager _adsManager;
		[Inject] private readonly ISceneLoader _sceneLoader;
		
		protected override void HandleInit()
		{
			var mainContext = FindObjectOfType<SceneContext<MainScene>>();
			_adsManager.Init();
			_sceneLoader.Init();

			_sceneLoader.LoadScene(new MenuScene(), LoadSceneMode.Additive, mainContext);
		}
	}
}