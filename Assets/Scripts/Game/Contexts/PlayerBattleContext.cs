﻿using Code.Game;
using Code.Static;
using Photon.Pun;
using UnityEngine;

namespace Code.Framework
{
	public class PlayerBattleContext : SceneContext<BattleScene>
	{
		[SerializeField] private GridData _gridData;
		[SerializeField] private CellUiView _cellPrefabUiView;
		[SerializeField] private Updater _updater;
		[SerializeField] private Coroutines _coroutines;
		[SerializeField] private MultiplayerActions _photonViewer;

		protected override void HandleInit(Binder binder)
		{
			binder.AsInstance<GridModel>();
			binder.AsInstance<GridUiViewController>();
			binder.AsInstance<CellGeneratorUiViewController>();
			binder.AsInstance<InputUiViewController>();
			binder.AsInstance<NonGridFigureFactory>();
			binder.AsInstance<CellFactory>();
			binder.AsInstance<GridViewModel>();
			binder.AsInstance<TurnController>();
			binder.AsInstance<TurnMover>();
			binder.AsInstance<CurrentPlayerChanger>();
			binder.AsInstance<EndGameObserver>();
			binder.AsInstance<FigureMover>();
			binder.AsInstance<GridUtils>();
			binder.AsInterface<NoteSender, IMessenger>();
			binder.AsInstance<FigureAnimator>();
			binder.AsInstance<FigureListener>();
			binder.AsInstance<BattleHudUiViewController>();
			binder.AsInjectFactory<CellUiViewController>();
			binder.AsInjectFactory<Figure>();
			binder.AsInjectFactory<PlayerInfoUiViewController>();
			binder.AsInstance<AiDoTurnMover>();

			binder.AsSerializedField(_gridData);
			binder.AsSerializedField(_cellPrefabUiView);

			binder.AsSerializedField<IUpdater>(_updater);
			binder.AsSerializedField<ICoroutines>(_coroutines);

			if (PhotonNetwork.InRoom)
			{
				binder.AsInterface<MultiplayerHolder, IPlayersHolder>();
				binder.AsSerializedField<IPlayerActions>(_photonViewer);
			}
			else
			{
				binder.AsInterface<SinglePlayerActions, IPlayerActions>();
				binder.AsInterface<SinglePlayersHolder, IPlayersHolder>();
			}
		}
	}
}