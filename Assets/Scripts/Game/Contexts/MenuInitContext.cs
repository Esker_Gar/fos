﻿using Code.Game;
using UnityEngine;

namespace Code.Framework
{
	public class MenuInitContext : InitContext
	{
		[SerializeField] private MenuHudUiView _menuHudUiView;

		[Inject] private readonly Factory<MenuHudUiViewController> _menuHudUiViewControllerFactory;
		[Inject] private readonly LocalData _localData;
		[Inject] private readonly AdsManager _adsManager;
		[Inject] private readonly LobbyManager _lobbyManager;

		protected override void HandleInit()
		{
			_lobbyManager.ConnectToServer();
			_localData.Init();

			var menuHudController = _menuHudUiViewControllerFactory.Create();
			menuHudController.SetView(_menuHudUiView);
			menuHudController.Init();
			
			_adsManager.LoadAds();
		}
	}
}