﻿using Code.Game;

namespace Code.Framework
{
	public class MenuContext : SceneContext<MenuScene>
	{
		protected override void HandleInit(Binder binder)
		{
			binder.AsInjectFactory<MenuHudUiViewController>();
			binder.AsInstance<LocalData>();
			binder.AsInjectFactory<ChangeNicknameUiViewController>();
		}
	}
}