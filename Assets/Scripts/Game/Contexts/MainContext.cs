﻿using Code.Game;
using Code.Static;
using UnityEngine;

namespace Code.Framework
{
	public class MainContext : SceneContext<MainScene>
	{
		[SerializeField] private Configs _configs;
		[SerializeField] private LobbyManager _lobbyManager;
		
		protected override void HandleInit(Binder binder)
		{
			binder.AsInstance<PlayerData>();
			binder.AsInstance<AdsManager>();
			binder.AsInterface<SceneLoader, ISceneLoader>();
			
			binder.AsSerializedField(_configs);
			binder.AsSerializedField(_lobbyManager);
		}
	}
}