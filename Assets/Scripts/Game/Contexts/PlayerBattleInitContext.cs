﻿using Code.Game;
using Code.Static;
using Photon.Pun;
using UnityEngine;

namespace Code.Framework
{
	public class PlayerBattleInitContext : InitContext
	{
		[SerializeField] private GridUiView _gridUiView;
		[SerializeField] private CellGeneratorUiView _cellGeneratorUiView;
		[SerializeField] private InputUiView _inputUiView;
		[SerializeField] private BattleHudUiView _battleHudUiView;
		
		[Inject] private readonly GridData _gridData;
		[Inject] private readonly GridModel _gridModel;
		[Inject] private readonly GridUiViewController _gridUiViewController;
		[Inject] private readonly CellGeneratorUiViewController _cellGeneratorController;
		[Inject] private readonly InputUiViewController _inputUiViewController;
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly IPlayersHolder _playersHolder;
		[Inject] private readonly EndGameObserver _endGameObserver;
		[Inject] private readonly IPlayerActions _playerActions;
		[Inject] private readonly TurnMover _doTurnMover;
		[Inject] private readonly BattleHudUiViewController _battleHudUiViewController;
		[Inject] private readonly AiDoTurnMover _aiDoTurnMover;
		[Inject] private readonly IMessenger _messenger;
		[Inject] private readonly FigureListener _createFigureListener;
		[Inject] private readonly TurnController _turnController;

		protected override void HandleInit()
		{
			_playerActions.Init();
			_gridModel.Init(new AbstractGrid.InitData(_gridData.Width, _gridData.Height));
			
			_gridUiViewController.SetView(_gridUiView);
			_gridUiViewController.Init();

			_cellGeneratorController.SetView(_cellGeneratorUiView);
			_cellGeneratorController.Init();

			_playersHolder.Init();
			_currentPlayerChanger.Init();

			_inputUiViewController.SetView(_inputUiView);
			_inputUiViewController.Init();
			
			_battleHudUiViewController.SetView(_battleHudUiView);
			_battleHudUiViewController.Init();
			
			_doTurnMover.Init();

			if (PhotonNetwork.InRoom == false)
			{
				_aiDoTurnMover.Init();
			}
			
			_endGameObserver.Init();
			_messenger.Init();
			_createFigureListener.Init();
			_turnController.Init();
		}
	}
}