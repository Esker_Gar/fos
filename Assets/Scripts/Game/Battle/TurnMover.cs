﻿using System;
using Code.Framework;

namespace Code.Game
{
	public class TurnMover : IInitializable
	{
		[Inject] private readonly TurnController _turnController;
		[Inject] private readonly IPlayerActions _playerActions;

		public event Action<AbstractGrid> OnPlayerCreateFigure;
		public event Action OnConfirmFigure;

		public void Init()
		{
			_playerActions.OnPlayersReady += DoFirstTurn;
		}

		public bool TryConfirmFigure()
		{
			var resultValue = _turnController.TryConfirmFigure();

			if (resultValue)
			{
				OnConfirmFigure?.Invoke();
			}
			
			return resultValue;
		}

		public void StartTurn()
		{
			_turnController.StartTurn((currentPlayer, figure) =>
			{
				_turnController.CreatePlayerFigure(currentPlayer, figure);
				OnPlayerCreateFigure?.Invoke(figure);
			});
		}

		private void DoFirstTurn()
		{
			_playerActions.OnPlayersReady -= DoFirstTurn;
			
			_playerActions.DoFirstTurn(StartTurn);
		}
	}
}