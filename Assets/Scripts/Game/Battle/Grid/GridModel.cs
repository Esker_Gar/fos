﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Code.Game
{
	public class GridModel : AbstractGrid
	{
		public Cell LeftSideInitCell => Grid[1, 1];
		public Cell RightSideInitCell => Grid[Height, Width];
		public GridFigure CurrentFigure {get; private set;}
		public List<Cell> PossibleInitialCells {get;} = new List<Cell>();
		public event Action OnAllGridBusy;
		public event Action<IEnumerable<Cell>> OnTakenAllSelectedCells;

		private int _busiesCellCount;

		public bool CheckPotentialInitCell(Cell initCell, int iterationStep, AbstractGrid figure)
		{
			var heightWithInitialCell = figure.Height - 1;
			var widthWithInitialCell = figure.Width - 1;
			var maxVerticalCellIndex = initCell.VerticalIndex + heightWithInitialCell * iterationStep;
			var maxHorizontalCellIndex = initCell.HorizontalIndex + widthWithInitialCell * iterationStep;

			for (int i = initCell.VerticalIndex; Condition(i, maxVerticalCellIndex, iterationStep); i += iterationStep)
			{
				for (int j = initCell.HorizontalIndex; Condition(j, maxHorizontalCellIndex, iterationStep); j += iterationStep)
				{
					var cell = Grid[i, j];

					if (i > Height || j > Width || i <= 0 || j <= 0 || cell.State != CellState.Free)
					{
						return false;
					}
				}
			}
			
			PossibleInitialCells.Add(initCell);

			return true;
		}

		public void CreateFigure(Cell initCell, int iterationStep, int figureWidth, int figureHeight)
		{
			CurrentFigure = new GridFigure(figureWidth, figureHeight);
			
			var initVerticalIndex = initCell.VerticalIndex;
			var initHorizontalIndex = initCell.HorizontalIndex;
			var maxVerticalCellIndex = initVerticalIndex + --figureHeight * iterationStep;
			var maxHorizontalCellIndex = initHorizontalIndex + --figureWidth * iterationStep;
			var horizontalLimitValue = iterationStep > 0 ? Width : 1;
			var verticalLimitValue = iterationStep > 0 ? Height : 1;

			if (maxHorizontalCellIndex > Width || maxHorizontalCellIndex < 1)
			{
				initHorizontalIndex -= maxHorizontalCellIndex - horizontalLimitValue;
				maxHorizontalCellIndex = horizontalLimitValue;
			}

			if (maxVerticalCellIndex > Height || maxVerticalCellIndex < 1)
			{
				initVerticalIndex -= maxVerticalCellIndex - verticalLimitValue;
				maxVerticalCellIndex = verticalLimitValue;
			}

			for (int i = initVerticalIndex; Condition(i, maxVerticalCellIndex, iterationStep); i += iterationStep)
			{
				for (int j = initHorizontalIndex; Condition(j, maxHorizontalCellIndex, iterationStep); j += iterationStep)
				{
					var cell = Grid[i, j];

					CurrentFigure.AddCell(cell);
				}
			}
			
			PossibleInitialCells.Clear();
		}

		public void TakeAllSelectedCells(IEnumerable<Cell> selectedCells)
		{
			var cells = selectedCells as Cell[] ?? selectedCells.ToArray();
			
			foreach (var cell in cells)
			{
				this[cell].State = CellState.Busy;
				_busiesCellCount++;
			}
			
			CurrentFigure = null;

			if (_busiesCellCount == Height * Width)
			{
				OnAllGridBusy?.Invoke();
			}
			
			OnTakenAllSelectedCells?.Invoke(cells);
		}

		public void ClearAllSelectedCells()
		{
			if (CurrentFigure != null)
			{
				var cells = CurrentFigure.Cells;
			
				foreach (var cell in cells)
				{
					this[cell].State = this[cell].PreviousState;
				}
			
				CurrentFigure = null;
			}
		}

		public bool TryGetNeighbourIndex(Cell previousCell, Cell cell, out int index)
		{
			var neighbours = GetNeighbours(previousCell);

			for (int i = 0; i < neighbours.Length; i++)
			{
				var neighbour = neighbours[i];

				if (neighbour.Equals(cell))
				{
					index = i;

					return true;
				}
			}

			index = default;

			return false;
		}

		public Cell[] GetAxisNeighbours(Cell cell)
		{
			var neighbours = new Cell[4];

			neighbours[0] = Grid[cell.VerticalIndex - 1, cell.HorizontalIndex];
			neighbours[1] = Grid[cell.VerticalIndex, cell.HorizontalIndex + 1];
			neighbours[2] = Grid[cell.VerticalIndex + 1, cell.HorizontalIndex];
			neighbours[3] = Grid[cell.VerticalIndex, cell.HorizontalIndex - 1];

			return neighbours;
		}

		private bool Condition(int iterateValue, int maxCellIndex, int iterationStep)
		{
			if (iterationStep > 0)
			{
				return iterateValue <= maxCellIndex;
			}
			else
			{
				return iterateValue >= maxCellIndex;
			}
		}
	}
}