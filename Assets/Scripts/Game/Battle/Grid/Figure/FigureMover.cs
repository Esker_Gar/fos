﻿using System;
using System.Collections.Generic;
using Code.Framework;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Game
{
	public class FigureMover
	{
		[Inject] private readonly GridViewModel _gridViewModel;
		[Inject] private readonly GridModel _gridModel;

		public event Action OnFigureMoved;
		
		private CellUiView _previousCell;

		public void Move(Vector2 position)
		{
			var pointerEventData = new PointerEventData(EventSystem.current)
			{
				position = position
			};

			var raycastResults = new List<RaycastResult>(1);
			EventSystem.current.RaycastAll(pointerEventData, raycastResults);

			if (raycastResults.Count <= 0)
			{
				return;
			}
			
			foreach (var result in raycastResults)
			{
				var cell = result.gameObject.GetComponent<CellUiView>();

				if (cell == null)
				{
					return;
				}

				if (_previousCell != null &&
				    _previousCell.Equals(cell) == false &&
				    _gridViewModel.CellUiViewControllers.TryGetValue(cell, out var cellController) &&
				    _gridViewModel.CellUiViewControllers.ContainsKey(_previousCell))
				{
					var cellModel = cellController.Cell;
					var previousCellController = _gridViewModel.CellUiViewControllers[_previousCell];
					var previousCellModel = previousCellController.Cell;

					if (_gridModel.TryGetNeighbourIndex(previousCellModel, cellModel, out var index))
					{
						var currenGridFigure = _gridModel.CurrentFigure;

						if (currenGridFigure == null)
						{
							return;
						}

						var figureCells = new List<Cell>(currenGridFigure.Cells);

						figureCells.Reverse();

						if (CheckBorders(currenGridFigure.Cells, index) == false)
						{
							return;
						}

						MoveFigure(figureCells, index, currenGridFigure);
					}
				}
				
				_previousCell = cell;
			}
		}

		private bool CheckBorders(IEnumerable<Cell> cells, int index)
		{
			foreach (var figureCell in cells)
			{
				var neighbours = _gridModel.GetNeighbours(figureCell);
				var neighbour = neighbours[index];

				if (neighbour.HorizontalIndex > _gridModel.Width || neighbour.VerticalIndex > _gridModel.Height ||
				    neighbour.HorizontalIndex <= 0 || neighbour.VerticalIndex <= 0)
				{
					return false;
				}
			}

			return true;
		}

		private void MoveFigure(IEnumerable<Cell> figureCells, int index, GridFigure currenGridFigure)
		{
			foreach (var figureCell in figureCells)
			{
				var neighbours = _gridModel.GetNeighbours(figureCell);
				var needNeighbour = neighbours[index];
				figureCell.State = figureCell.PreviousState;

				currenGridFigure.RemoveCell(figureCell);
				currenGridFigure.AddCell(needNeighbour);
			}
			
			foreach (var figureCell in currenGridFigure.Cells)
			{
				figureCell.State = figureCell.State == CellState.Busy ? CellState.Blocked : CellState.Select;
			}
			
			OnFigureMoved?.Invoke();
		}
	}
}