﻿using Code.Framework;
using Random = UnityEngine.Random;

namespace Code.Game
{
	public class NonGridFigureFactory
	{
		[Inject] private readonly Factory<Figure> _figureFactory;

		private int _maxWidth = 7;
		private int _maxHeight = 7;

		public AbstractGrid Create()
		{
			var width = Random.Range(1, _maxWidth);
			var height = Random.Range(1, _maxHeight);

			var figure = _figureFactory.Create();

			figure.Init(new AbstractGrid.InitData(width, height));

			return figure;
		}

		public void RefreshMaxValues(AbstractGrid figure)
		{
			if (figure.Height >= figure.Width && _maxHeight > 1)
			{
				_maxHeight--;
			}
			else
			{
				_maxWidth--;
			}
		}
	}
}