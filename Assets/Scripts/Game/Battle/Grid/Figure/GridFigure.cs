﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Game
{
	public class GridFigure
	{
		public int Width {get;}
		public int Height {get;}
		public Cell LastCell => _cells[_cells.Count - 1];
		public IEnumerable<Cell> Cells => _cells;

		private readonly List<Cell> _cells;

		public GridFigure(int width, int height)
		{
			Width = width;
			Height = height;
			_cells = new List<Cell>();
		}

		public void AddCell(Cell cell)
		{
			_cells.Add(cell);
		}

		public Vector2 GetMaxCellsValues()
		{
			var maxWidthValue = 0;
			var maxHeightValue = 0;

			foreach (var cell in _cells)
			{
				if (cell.HorizontalIndex >= maxWidthValue && cell.VerticalIndex >= maxHeightValue)
				{
					maxWidthValue = cell.HorizontalIndex;
					maxHeightValue = cell.VerticalIndex;
				}
			}

			return new Vector2(maxWidthValue, maxHeightValue);
		}
		
		public Vector2 GetMinCellsValues()
		{
			var minWidthValue = int.MaxValue;
			var minHeightValue = int.MaxValue;

			foreach (var cell in _cells)
			{
				if (cell.HorizontalIndex <= minWidthValue && cell.VerticalIndex <= minHeightValue)
				{
					minWidthValue = cell.HorizontalIndex;
					minHeightValue = cell.VerticalIndex;
				}
			}

			return new Vector2(minWidthValue, minHeightValue);
		}

		public void RemoveCell(Cell cell)
		{
			if (ContainCell(cell))
			{
				_cells.Remove(cell);
			}
		}
		
		private bool ContainCell(Cell cell)
		{
			return _cells.Contains(cell);
		}
	}
}