﻿using Code.Framework;

namespace Code.Game
{
	public class AbstractGrid : IInitializable<AbstractGrid.InitData>
	{
		public readonly struct InitData
		{
			public readonly int Width;
			public readonly int Height;

			public InitData(int width, int height)
			{
				Width = width;
				Height = height;
			}
		}

		public Cell this[int i, int j] => Grid[i + 1, j + 1];

		public Cell this[Cell cell] => Grid[cell.VerticalIndex, cell.HorizontalIndex];
		public int Width => _width - 2;
		public int Height => _height - 2;

		private int _width;
		private int _height;
		protected Cell[,] Grid;

		public void Init(InitData initData)
		{
			_width = initData.Width + 2;
			_height = initData.Height + 2;

			Grid = new Cell[_height, _width];

			for (int i = 0; i < _height; i++)
			{
				for (int j = 0; j < _width; j++)
				{
					if (i == 0 || i == _height - 1 || j == 0 || j == _width - 1)
					{
						Grid[i, j] = new Cell(j, i, CellState.Border);
					}
					else
					{
						Grid[i, j] = new Cell(j, i);
					}
				}
			}
		}

		public Cell[] GetNeighbours(Cell cell)
		{
			var neighbours = new Cell[8];

			neighbours[0] = Grid[cell.VerticalIndex - 1, cell.HorizontalIndex];
			neighbours[1] = Grid[cell.VerticalIndex - 1, cell.HorizontalIndex + 1];
			neighbours[2] = Grid[cell.VerticalIndex, cell.HorizontalIndex + 1];
			neighbours[3] = Grid[cell.VerticalIndex + 1, cell.HorizontalIndex + 1];
			neighbours[4] = Grid[cell.VerticalIndex + 1, cell.HorizontalIndex];
			neighbours[5] = Grid[cell.VerticalIndex + 1, cell.HorizontalIndex - 1];
			neighbours[6] = Grid[cell.VerticalIndex, cell.HorizontalIndex - 1];
			neighbours[7] = Grid[cell.VerticalIndex - 1, cell.HorizontalIndex - 1];

			return neighbours;
		}
	}
}