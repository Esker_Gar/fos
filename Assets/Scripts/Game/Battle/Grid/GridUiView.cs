﻿using System;
using System.Collections.Generic;
using Code.Framework;
using Code.Static;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Code.Game
{
	public class GridUiView : UiView
	{
		[FormerlySerializedAs("_parent")]
		[SerializeField] private RectTransform _rectTransform;
		[SerializeField] private GridLayoutGroup _gridLayout;

		public RectTransform RectTransform => _rectTransform;
		public GridLayoutGroup GridLayout => _gridLayout;
	}

	public class GridViewModel
	{
		[Inject] private readonly GridModel _gridModel;
		[Inject] private readonly CellFactory _cellFactory;

		public int Width => _gridModel.Width;
		public int Height => _gridModel.Height;
		public IReadOnlyDictionary<CellUiView, CellUiViewController> CellUiViewControllers => _cellsByControllers;
		public IReadOnlyDictionary<Cell, CellUiView> ModelCellsByViews => _modelCellsByViews;

		private readonly Dictionary<CellUiView, CellUiViewController> _cellsByControllers = new Dictionary<CellUiView, CellUiViewController>();
		private readonly Dictionary<Cell, CellUiView> _modelCellsByViews = new Dictionary<Cell, CellUiView>();

		public void AddCell(int i, int j, RectTransform parent)
		{
			var cell = _gridModel[i, j];
			var (cellUiView, cellUiViewController) = _cellFactory.Create(parent, cell);
			_cellsByControllers.Add(cellUiView, cellUiViewController);
			_modelCellsByViews.Add(cell, cellUiView);
		}
	}

	public class GridUiViewController : GenericController<GridUiView>
	{
		[Inject] private readonly GridData _gridData;
		[Inject] private readonly GridViewModel _gridViewModel;
		[Inject] private readonly CellUiView _cellUiViewPrefab;

		protected override void HandleInit()
		{
			MutableView.GridLayout.constraintCount = Mathf.RoundToInt(_gridData.Height);
			MutableView.GridLayout.cellSize = _cellUiViewPrefab.RectTransform.sizeDelta;

			for (int i = 0; i < _gridViewModel.Height; i++)
			{
				for (int j = 0; j < _gridViewModel.Width; j++)
				{
					_gridViewModel.AddCell(i, j, MutableView.RectTransform);
				}
			}
		}
	}
}