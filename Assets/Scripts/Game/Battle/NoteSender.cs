﻿using System;
using Code.Framework;
using Code.Static;

namespace Code.Game
{
	public class NoteSender : IMessenger, IDisposable
	{
		private enum Note
		{
			GenerateFigure,
			SpotSelection,
			OpponentTurn
		}
		
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly TurnMover _doTurnMover;
		[Inject] private readonly Configs _configs;

		public event Action<string> OnMessage;

		public void Init()
		{
			_currentPlayerChanger.OnPlayerChanged += OnPlayerChanged;
			_doTurnMover.OnPlayerCreateFigure += OnAddFigure;
		}
		
		public void Dispose()
		{
			_currentPlayerChanger.OnPlayerChanged -= OnPlayerChanged;
			_doTurnMover.OnPlayerCreateFigure -= OnAddFigure;
		}

		private void OnPlayerChanged()
		{
			SendNote(_currentPlayerChanger.CurrentPlayer.IsPlayerTurn ? Note.GenerateFigure : Note.OpponentTurn);
		}

		private void OnAddFigure(AbstractGrid figure)
		{
			SendNote(Note.SpotSelection);
		}
		
		private void SendNote(Note note)
		{
			var configPhrases = _configs.ConfigPhrases;
			var message = note switch
			{
				Note.GenerateFigure => configPhrases.NoteGenerateFigure,
				Note.SpotSelection => configPhrases.NoteSpotSelection,
				Note.OpponentTurn => configPhrases.NoteOpponentTurn,
				_ => string.Empty
			};

			OnMessage?.Invoke(message);
		}
	}
}