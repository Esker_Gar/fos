﻿using System.Collections.Generic;
using System.Linq;
using Code.Framework;
using Code.Static;
using DG.Tweening;

namespace Code.Game
{
	public enum FigureAnimation
	{
		Create,
		Add
	}
	
	public class FigureAnimator
	{
		[Inject] private readonly GridViewModel _gridViewModel;
		[Inject] private readonly Configs _configs;

		private Sequence _createdFigureAnimation;
		private readonly List<Sequence> _addedFigureAnimations = new List<Sequence>();

		public void StopAppearingGeneratedFigureOnGridAnimation(bool completeAnimation, FigureAnimation figureAnimation)
		{
			switch (figureAnimation)
			{
				case FigureAnimation.Create:
					_createdFigureAnimation.Kill(completeAnimation);
					break;
				case FigureAnimation.Add:
					foreach (var addedFigureAnimation in _addedFigureAnimations)
					{
						addedFigureAnimation.Kill(completeAnimation);
					}
					
					_addedFigureAnimations.Clear();
					break;
			}
		}

		public void AppearingFigureOnGridAnimation(IEnumerable<Cell> cells, FigureAnimation figureAnimation)
		{
			switch (figureAnimation)
			{
				case FigureAnimation.Create:
					_createdFigureAnimation = CreateAppearingCellsOnGridAnimation(cells).Play();
					break;
				case FigureAnimation.Add:
					_addedFigureAnimations.Add(CreateAppearingCellsOnGridAnimation(cells).Play());
					break;
			}
		}
		
		private Sequence CreateAppearingCellsOnGridAnimation(IEnumerable<Cell> cells)
		{
			var sequence = DOTween.Sequence();
			var configAnimations = _configs.ConfigAnimations;
			var cellsArray = cells as Cell[] ?? cells.ToArray();
			var durationForOneCell = configAnimations.CellsColorAnimationDuration / cellsArray.Length;
			
			foreach (var cell in cellsArray)
			{
				if (_gridViewModel.ModelCellsByViews.TryGetValue(cell, out var cellUiView) && 
				    _gridViewModel.CellUiViewControllers.TryGetValue(cellUiView, out var cellUiViewController))
				{
					sequence.Append(cellUiViewController.GetColorAnimation(durationForOneCell));
				}
			}

			return sequence;
		}
	}
}