﻿using System;
using Code.Framework;
using Code.Static;

namespace Code.Game
{
	public class SinglePlayerActions : IPlayerActions
	{
		[Inject] private readonly GridModel _gridModel;
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly Configs _configs;

		public Action OnPlayersReady {get; set;}
		public Action<PlayerInfoData> OnInitPlayerInfoData {get; set;}

		public void DoFirstTurn(Action action)
		{
			action?.Invoke();
		}

		public void InitOppositePlayerInfoData()
		{
			var aiPlayer = new PlayerInfoData(_configs.ConfigPhrases.BotName, 0, 0, 0);
			OnInitPlayerInfoData?.Invoke(aiPlayer);
		}

		public void Init()
		{
		}

		public void TakeAllSelectedCells(Cell[] cells)
		{
			_currentPlayerChanger.CurrentPlayerColor = _currentPlayerChanger.CurrentPlayer.Color;
			_gridModel.TakeAllSelectedCells(cells);
		}

		public void ChangePlayer()
		{
			_currentPlayerChanger.ChangePlayer();
		}

		public void CreateGridFigure(Cell initCell, int iterationStep, int figureWidth, int figureHeight)
		{
			_gridModel.CreateFigure(initCell, iterationStep, figureWidth, figureHeight);
		}

		public void PlayerReady()
		{
			OnPlayersReady.Invoke();
		}
	}
}