﻿using Code.Framework;
using Code.Static;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Game
{
	public class CellUiView : UiView
	{
		[SerializeField] private RectTransform _rectTransform;
		[SerializeField] private Image _cellImage;

		public RectTransform RectTransform => _rectTransform;
		public Image CellImage => _cellImage;

		public void SetColor(Color color)
		{
			_cellImage.color = color;
		}
	}

	public class CellUiViewController : GenericController<CellUiView, CellUiViewController.InitData>
	{
		public readonly struct InitData
		{
			public readonly Cell Cell;

			public InitData(Cell cell)
			{
				Cell = cell;
			}
		}

		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly Configs _configs;

		public Cell Cell {get; private set;}
		
		private Color _busyColor = Color.clear;
		private Color _previousColor;
		private Color _currentColor;
		private Tween _appearingTween;

		protected override void HandleInit(InitData initData)
		{
			var configColors = _configs.ConfigColors;
			Cell = initData.Cell;

			initData.Cell.OnStateChanged += ChangeViewState;
			MutableView.SetColor(configColors.DefaultCellColor);
			_currentColor = configColors.DefaultCellColor;
		}

		protected override void HandleDestroy()
		{
			Cell.OnStateChanged -= ChangeViewState;
			_appearingTween.Kill();
		}

		public Tween GetPunchScaleAnimation(float value, float duration)
		{
			var configAudio = _configs.ConfigAudio;
			
			return MutableView.transform.DOPunchScale(Vector3.one * value, duration, 1, 0)
			                  .OnStart(() => MutableView.SetActive(true))
			                  .OnPlay(() => AudioPlayer.PlaySfxShot(configAudio.CreateCell));
		}

		public Tween GetColorAnimation(float duration)
		{
			MutableView.SetColor(_previousColor);
			return MutableView.CellImage.DOColor(_currentColor, duration);
		}

		private void ChangeViewState(CellState cellState)
		{
			if (cellState == CellState.Busy && _busyColor == Color.clear)
			{
				_busyColor = _currentPlayerChanger.CurrentPlayerColor;
			}

			var configColors = _configs.ConfigColors;
			var color = default(Color);

			switch (cellState)
			{
				case CellState.Free:
					color = configColors.DefaultCellColor;
					break;
				case CellState.Select:
					color = configColors.AvailableCellsColor;
					break;
				case CellState.Busy:
					color = _busyColor;
					break;
				case CellState.Blocked:
					color = configColors.BlockedCellsColor;
					break;
			}

			_previousColor = _currentColor;
			_currentColor = color;
			
			MutableView.SetColor(color);
		}
	}
}