﻿namespace Code.Game
{
	public interface ICellClickHandler
	{
		void ClickOnCell(CellUiViewController cellUiViewController);
	}
}