﻿using System;

namespace Code.Game
{

	public enum CellState
	{
		Free,
		Select,
		Border,
		Busy,
		Blocked
	}

	public class Cell
	{
		public readonly int HorizontalIndex;
		public readonly int VerticalIndex;
		public CellState State
		{
			get => _state;
			set => ChangeCellState(value);
		}
		public CellState PreviousState {get; private set;}

		public event Action<CellState> OnStateChanged;

		private CellState _state;

		public Cell(int horizontalIndex, int verticalIndex, CellState state = CellState.Free)
		{
			HorizontalIndex = horizontalIndex;
			VerticalIndex = verticalIndex;
			State = state;
		}

		private void ChangeCellState(CellState cellState)
		{
			if (_state != CellState.Select)
			{
				PreviousState = _state;
			}

			_state = cellState;
			OnStateChanged?.Invoke(_state);
		}
	}
}