﻿using System;
using Code.Framework;
using UnityEngine;

namespace Code.Game
{
	public class CellFactory
	{
		[Inject] private readonly CellUiView _cellPrefabUiView;
		[Inject] private readonly Factory<CellUiViewController> _cellUiViewControllerFactory;

		public Tuple<CellUiView, CellUiViewController> Create(Transform parent, Cell cell)
		{
			var cellUiView = _cellPrefabUiView.Instantiate(parent);
			var cellUiViewController = _cellUiViewControllerFactory.Create();
			cellUiViewController.SetView(cellUiView);
			cellUiViewController.Init(new CellUiViewController.InitData(cell));

			return new Tuple<CellUiView, CellUiViewController>(cellUiView, cellUiViewController);
		}
	}
}