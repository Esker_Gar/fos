﻿namespace Code.Game
{
	public struct CellUtils
	{
		public bool TryGetBorderCell(AbstractGrid grid, Cell cell, out int turns)
		{
			turns = -1;

			var neighbours = grid.GetNeighbours(cell);

			if (neighbours[0].State == CellState.Border)
			{
				turns = 0;
			}
			else if (neighbours[2].State == CellState.Border)
			{
				turns = 1;
			}
			else if (neighbours[4].State == CellState.Border)
			{
				turns = 2;
			}
			else if(neighbours[6].State == CellState.Border)
			{
				turns = 3;
			}

			return turns != -1;
		}

		public bool TryGetEdgeCell(AbstractGrid grid, Cell cell, out int turns)
		{
			turns = -1;

			var neighbours = grid.GetNeighbours(cell);

			if (neighbours[7].State == CellState.Border && neighbours[6].State == CellState.Border && neighbours[0].State == CellState.Border)
			{
				turns = 0;
			}
			else if (neighbours[1].State == CellState.Border && neighbours[2].State == CellState.Border && neighbours[0].State == CellState.Border)
			{
				turns = 1;
			}
			else if (neighbours[3].State == CellState.Border && neighbours[2].State == CellState.Border && neighbours[4].State == CellState.Border)
			{
				turns = 2;
			}
			else if (neighbours[5].State == CellState.Border && neighbours[6].State == CellState.Border && neighbours[4].State == CellState.Border)
			{
				turns = 3;
			}

			return turns != -1;
		}
	}
}