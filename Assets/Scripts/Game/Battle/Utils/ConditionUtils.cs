﻿namespace Code.Game
{
	public readonly struct ConditionUtils
	{
		private readonly Direction _direction;
		
		public ConditionUtils(Direction direction)
		{
			_direction = direction;
		}
		
		public bool CheckNonStrictCondition(int valueA, int valueB)
		{
			if (_direction == Direction.DownRight)
			{
				return valueA <= valueB;
			}
			else
			{
				return valueA >= valueB;
			}
		}
		
		public bool CheckStrictCondition(int valueA, int valueB)
		{
			if (_direction == Direction.DownRight)
			{
				return valueA < valueB;
			}
			else
			{
				return valueA > valueB;
			}
		}
	}
}