﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Framework;

namespace Code.Game
{
	public class GridUtils
	{
		[Inject] private readonly GridModel _gridModel;
		[Inject] private readonly IPlayerActions _playerActions;

		public GridFigure CurrentFigure => _gridModel.CurrentFigure;
		public List<Cell> PossibleInitialCells => _gridModel.PossibleInitialCells;

		public bool CheckBlockedCells()
		{
			if (CurrentFigure == null)
			{
				return false;
			}

			foreach (var cell in CurrentFigure.Cells)
			{
				if (cell.State == CellState.Blocked)
				{
					return false;
				}
			}

			return true;
		}

		public void ClearAllSelectedCells()
		{
			_gridModel.ClearAllSelectedCells();
		}

		public bool CheckOpportunityToAddFigure(IEnumerable<Cell> cells, int iterationStep, AbstractGrid figure)
		{
			bool Condition(Cell cell)
			{
				return _gridModel.CheckPotentialInitCell(cell, iterationStep, figure);
			}

			return CheckFigureNeighbours(cells, Condition);
		}

		private bool CheckFigureNeighbours(IEnumerable<Cell> cells, Func<Cell, bool> condition)
		{
			foreach (var cell in cells)
			{
				if (CheckNeighbourCellToAddFigure(cell, condition))
				{
					return true;
				}
			}

			return false;
		}

		public bool CheckNeighbourCellToAddFigure(Cell cell, Func<Cell, bool> condition)
		{
			var axisNeighbours = _gridModel.GetAxisNeighbours(cell) as IList<Cell>;

			foreach (var neighbour in axisNeighbours)
			{
				if (condition(neighbour))
				{
					return true;
				}
			}

			return false;
		}

		public void SelectCells()
		{
			foreach (var cell in CurrentFigure.Cells)
			{
				cell.State = cell.State == CellState.Busy ? CellState.Blocked : CellState.Select;
			}
		}

		public void CheckFreeSpace(Player player)
		{
			var conditionUtils = new ConditionUtils(player.Direction);
			var freeCells = new HashSet<Cell>();
			var cellValue = player.GetCellValue();
			var iterationStep = (int)player.Direction;
			var verticalIndex = player.InitialCell.VerticalIndex;

			for (int i = verticalIndex; conditionUtils.CheckNonStrictCondition(i, (int)cellValue.y); i += iterationStep)
			{
				var horizontalIndex =  player.InitialCell.HorizontalIndex;
				
				if (player.HorizontalInitialIndex.TryGetValue(i, out var index))
				{
					horizontalIndex = index;
				}

				for (int j = horizontalIndex; conditionUtils.CheckNonStrictCondition(j, (int)cellValue.x); j += iterationStep)
				{
					var fakeCell = new Cell(j, i);
					var cell = _gridModel[fakeCell];

					if (cell.State == CellState.Free && freeCells.Contains(cell) == false)
					{
						freeCells.Add(cell);
						PassFreeCells(cell, freeCells, player);
					}
				}
			}

			RefreshHorizontalInitialIndex(player);
		}

		private void RefreshHorizontalInitialIndex(Player player)
		{
			var conditionUtils = new ConditionUtils(player.Direction);
			var cellValue = player.GetCellValue();
			var iterationStep = (int)player.Direction;
			var verticalIndex = player.InitialCell.VerticalIndex;

			for (int i = verticalIndex; conditionUtils.CheckStrictCondition(i, (int)cellValue.y); i += iterationStep)
			{
				var horizontalIndex =  player.InitialCell.HorizontalIndex;
				
				if (player.HorizontalInitialIndex.TryGetValue(i, out var index))
				{
					horizontalIndex = index;
				}

				for (int j = horizontalIndex; conditionUtils.CheckStrictCondition(j, (int)cellValue.x); j += iterationStep)
				{
					var fakeCell = new Cell(j, i);
					var cell = _gridModel[fakeCell];
					
					if (cell.State == CellState.Busy && player.Cells.Contains(cell))
					{
						player.AddHorizontalIndex(cell.VerticalIndex, cell.HorizontalIndex);
					}
					else if (cell.State == CellState.Free)
					{
						break;
					}
				}
			}
		}

		private void PassFreeCells(Cell cell, HashSet<Cell> freeCells, Player player)
		{
			var conditionUtils = new ConditionUtils(player.Direction);
			var queue = new Queue<Cell>();
			var cellValue = player.GetCellValue();
			var findingFreeCells = new HashSet<Cell>();
			var needClear = false;

			queue.Enqueue(cell);

			while (queue.Count > 0)
			{
				var dequeueCell = queue.Dequeue();
				var neighbours = _gridModel.GetAxisNeighbours(dequeueCell);
				findingFreeCells.Add(dequeueCell);

				foreach (var neighbour in neighbours)
				{
					if (neighbour.State == CellState.Free && freeCells.Contains(neighbour) == false)
					{
						if (conditionUtils.CheckNonStrictCondition(neighbour.HorizontalIndex, (int)cellValue.x) &&
						    conditionUtils.CheckNonStrictCondition(neighbour.VerticalIndex, (int)cellValue.y))
						{
							queue.Enqueue(neighbour);
							freeCells.Add(neighbour);
						}
						else
						{
							needClear = true;
						}
					}
					else if (neighbour.State == CellState.Busy && player.Cells.Contains(neighbour) == false)
					{
						needClear = true;
					}
				}
			}

			if (needClear)
			{
				findingFreeCells.Clear();
			}

			foreach (var freeCell in findingFreeCells)
			{
				player.AddCell(freeCell);
			}

			if (findingFreeCells.Count > 0)
			{
				_playerActions.TakeAllSelectedCells(findingFreeCells.ToArray());
			}
		}
	}
}