﻿using System;

namespace Code.Game
{
	public static class GridExtensions
	{
		public static void PassGrid(this AbstractGrid grid, Action<Cell> action)
		{
			for (int i = 0; i < grid.Height; i++)
			{
				for (int j = 0; j < grid.Width; j++)
				{
					var cell = grid[i, j];
					
					action.Invoke(cell);
				}
			}
		}
	}
}