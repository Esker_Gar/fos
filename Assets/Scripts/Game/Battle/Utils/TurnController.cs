﻿using System;
using System.Linq;
using Code.Framework;
using Code.Static;
using Random = UnityEngine.Random;

namespace Code.Game
{
	public class TurnController : IUpdatable, IInitializable
	{
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly IPlayerActions _playerActions;
		[Inject] private readonly GridUtils _gridUtils;
		[Inject] private readonly NonGridFigureFactory _nonGridFigureFactory;
		[Inject] private readonly IUpdater _updater;
		[Inject] private readonly Configs _configs;
		[Inject] private readonly EndGameObserver _endGameObserver;

		public float CurrentTurnTime => _currentTurnTime;

		private float _turnTime;
		private float _currentTurnTime;
		
		public void Init()
		{
			_turnTime = _configs.ConfigGame.TurnTime;
			_currentTurnTime = _turnTime;
			_updater.Attach(this);
			_currentPlayerChanger.OnPlayerChanged += () => _currentTurnTime = _turnTime;
			_endGameObserver.OnGameEnded += (_) => _updater.Disattach(this);
		}
		
		public void Update(float dt)
		{
			if (_currentTurnTime <= 0)
			{
				_gridUtils.ClearAllSelectedCells();
				_currentTurnTime = _turnTime;

				if (_currentPlayerChanger.LocalPlayer.IsPlayerTurn && _currentPlayerChanger.LocalPlayer.IsFirstTurn == false)
				{
					_playerActions.ChangePlayer();
				}
			}
			else if (_currentPlayerChanger.LocalPlayer.IsFirstTurn == false)
			{
				_currentTurnTime -= dt;
			}
		}

		public bool TryConfirmFigure()
		{
			if (_gridUtils.CheckBlockedCells() && CheckRightPlayerNeighbourFigure())
			{
				RefreshPlayerInitCell();
				AddCurrentFigure();

				return true;
			}

			return false;
		}

		public void StartTurn(Action<Player, AbstractGrid> addFigureAction)
		{
			var figure = _nonGridFigureFactory.Create();
			var currentPlayer = _currentPlayerChanger.CurrentPlayer;
			var iterationStep = (int) currentPlayer.Direction;
			var isCanAddFigure = false;

			if (currentPlayer.IsFirstTurn)
			{
				CreatePlayerFigure(currentPlayer, figure);
				AddCurrentFigure();
				currentPlayer.IsFirstTurn = false;
				_playerActions.ChangePlayer();
			}
			else
			{
				if (_gridUtils.CheckOpportunityToAddFigure(currentPlayer.Cells, iterationStep, figure))
				{
					isCanAddFigure = true;
				}

				if (isCanAddFigure)
				{
					addFigureAction?.Invoke(currentPlayer, figure);
				}
				else
				{
					_nonGridFigureFactory.RefreshMaxValues(figure);
					StartTurn(addFigureAction);
				}
			}
		}

		public void RefreshAiInitCell()
		{
			var possibleInitialCells = _gridUtils.PossibleInitialCells;
			var randomizeIndex = Random.Range(0, possibleInitialCells.Count);
			_currentPlayerChanger.CurrentPlayer.PreviousFigureInitCell = possibleInitialCells[randomizeIndex];
		}

		public void AddCurrentFigure()
		{
			var figure = _gridUtils.CurrentFigure;
			var player = _currentPlayerChanger.CurrentPlayer;
			
			_playerActions.TakeAllSelectedCells(figure.Cells.ToArray());
			player.RefreshPlayerCellInfo(figure);
			
			foreach (var cell in figure.Cells)
			{
				_currentPlayerChanger.CurrentPlayer.AddCell(cell);
			}
			
			_gridUtils.CheckFreeSpace(_currentPlayerChanger.CurrentPlayer);
		}


		public void CreatePlayerFigure(Player player, AbstractGrid figure)
		{
			var initialCell = player.PreviousFigureInitCell ?? player.InitialCell;
			
			_playerActions.CreateGridFigure(initialCell, (int) player.Direction, figure.Width, figure.Height);

			_gridUtils.SelectCells();
		}

		private void RefreshPlayerInitCell()
		{
			_currentPlayerChanger.CurrentPlayer.PreviousFigureInitCell = _gridUtils.CurrentFigure.LastCell;
		}

		private bool CheckRightPlayerNeighbourFigure()
		{
			bool Condition(Cell cell)
			{
				if (cell.State != CellState.Busy)
				{
					return false;
				}
				
				var playerGridFigures = _currentPlayerChanger.CurrentPlayer.Cells;

				return playerGridFigures.Contains(cell);
			}

			foreach (var cell in _gridUtils.CurrentFigure.Cells)
			{
				if (_gridUtils.CheckNeighbourCellToAddFigure(cell, Condition))
				{
					return true;
				}
			}

			return false;
		}
	}
}