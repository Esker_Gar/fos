﻿using Code.Framework;

namespace Code.Game
{
	public class AiDoTurnMover : IInitializable
	{
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly TurnController _turnController;
		[Inject] private readonly IPlayerActions _playerActions;
		[Inject] private readonly EndGameObserver _endGameObserver;

		public void Init()
		{
			_playerActions.OnPlayersReady += DoFirstTurn;
			_currentPlayerChanger.OnPlayerChanged += DoTurn;
			_endGameObserver.OnGameEnded += StopAiTurn;
		}

		private void StopAiTurn(GameResult gameResult)
		{
			_playerActions.OnPlayersReady -= DoFirstTurn;
			_currentPlayerChanger.OnPlayerChanged -= DoTurn;
			_endGameObserver.OnGameEnded -= StopAiTurn;
		}

		private void DoTurn()
		{
			if (_currentPlayerChanger.LocalPlayer.IsPlayerTurn == false)
			{
				_turnController.StartTurn((currentPlayer, figure) =>
				{
					_turnController.RefreshAiInitCell();
					_turnController.CreatePlayerFigure(currentPlayer, figure);
					_turnController.AddCurrentFigure();
					_playerActions.ChangePlayer();
				});
			}
		}

		private void DoFirstTurn()
		{
			_playerActions.DoFirstTurn(DoTurn);
		}
	}
}