﻿using System;
using Code.Framework;

namespace Code.Game
{
	public interface IPlayerActions : IInitializable
	{
		void TakeAllSelectedCells(Cell[] cells);
		void ChangePlayer();
		void CreateGridFigure(Cell initCell, int iterationStep, int figureWidth, int figureHeight);
		void PlayerReady();
		void DoFirstTurn(Action action);
		void InitOppositePlayerInfoData();
		Action OnPlayersReady {get; set;}
		Action<PlayerInfoData> OnInitPlayerInfoData {get; set;}
	}
}