﻿using System;
using UnityEngine;

namespace Code.Game
{
	public struct PlayerInfoData
	{
		public static PlayerInfoData Null => new PlayerInfoData(null, 0, 0, 0);

		public string Name;
		public int Victory;
		public int Draw;
		public int Defeat;

		public PlayerInfoData(string name, int victory, int draw, int defeat)
		{
			Name = name;
			Victory = victory;
			Draw = draw;
			Defeat = defeat;
		}

		public static bool operator ==(PlayerInfoData a, PlayerInfoData b)
		{
			return a.Name == b.Name && a.Victory == b.Victory && a.Defeat == b.Defeat && a.Draw == b.Draw;
		}

		public static bool operator !=(PlayerInfoData a, PlayerInfoData b)
		{
			return !(a == b);
		}
	}

	public class PlayerData
	{
		public event Action<PlayerInfoData> OnPlayerNameChanged;

		private const string PlayerNameKey = "PlayerName";
		private const string PlayerVictoryKey = "PlayerVictory";
		private const string PlayerDrawKey = "PlayerDraw";
		private const string PlayerDefeatKey = "PlayerDefeat";

		private PlayerInfoData _playerInfoData = PlayerInfoData.Null;

		public bool TryGetPlayerInfo(out PlayerInfoData playerInfoData)
		{
			if (_playerInfoData != PlayerInfoData.Null)
			{
				playerInfoData = _playerInfoData;
			}
			else
			{
				_playerInfoData = new PlayerInfoData();

				var name = PlayerPrefs.GetString(PlayerNameKey, string.Empty);
				var victory = PlayerPrefs.GetInt(PlayerVictoryKey, 0);
				var draw = PlayerPrefs.GetInt(PlayerDrawKey, 0);
				var defeat = PlayerPrefs.GetInt(PlayerDefeatKey, 0);

				if (string.IsNullOrEmpty(name) == false)
				{
					_playerInfoData = new PlayerInfoData(
						name,
						victory,
						draw,
						defeat);
				}

				playerInfoData = _playerInfoData;
			}

			return _playerInfoData != PlayerInfoData.Null;
		}

		public void SavePregameLose()
		{
			PlayerPrefs.SetInt(PlayerDefeatKey, ++_playerInfoData.Defeat);
		}

		public void SetPlayerName(string name)
		{
			PlayerPrefs.SetString(PlayerNameKey, name);

			_playerInfoData.Name = name;

			OnPlayerNameChanged?.Invoke(_playerInfoData);
		}

		public void RefreshStats(GameResult gameResult)
		{
			switch (gameResult)
			{
				case GameResult.Win:
					_playerInfoData.Victory++;
					_playerInfoData.Defeat--;
					PlayerPrefs.SetInt(PlayerVictoryKey, _playerInfoData.Victory);
					PlayerPrefs.SetInt(PlayerDefeatKey, _playerInfoData.Defeat);
					break;
				case GameResult.Draw:
					_playerInfoData.Draw++;
					_playerInfoData.Defeat--;
					PlayerPrefs.SetInt(PlayerDrawKey, _playerInfoData.Draw);
					PlayerPrefs.SetInt(PlayerDefeatKey, _playerInfoData.Defeat);
					break;
			}
		}
	}
}