﻿using Code.Framework;
using UnityEngine;

namespace Code.Game
{
	public class InputUiView : UiView
	{
		[SerializeField] private ButtonComponentView _generateButton;
		[SerializeField] private ButtonComponentView _confirmButton;
		[SerializeField] private ButtonComponentView _readyButton;

		public ButtonComponentView GenerateButton => _generateButton;
		public ButtonComponentView ConfirmButton => _confirmButton;
		public ButtonComponentView ReadyButton => _readyButton;
	}

	public class InputUiViewController : GenericController<InputUiView>, IUpdatable
	{
		[Inject] private readonly FigureMover _figureMover;
		[Inject] private readonly TurnMover _doTurnMover;
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly IPlayerActions _playerActions;
		[Inject] private readonly IUpdater _updater;
		[Inject] private readonly EndGameObserver _endGameObserver;

		protected override void HandleInit()
		{
			MutableView.GenerateButton.SetOnClick(OnGenerateButtonClick);
			MutableView.ConfirmButton.SetOnClick(OnConfirmButtonClick);
			MutableView.ReadyButton.SetOnClick(OnReadyButtonClick);
			
			_updater.Attach(this);
			_currentPlayerChanger.OnPlayerChanged += RefreshGenerateButton;
			_endGameObserver.OnGameEnded += DisableButtons;
			MutableView.GenerateButton.gameObject.SetActive(false);
			MutableView.ConfirmButton.gameObject.SetActive(false);
		}

		public void Update(float dt)
		{
			if (Input.GetMouseButton(0) && _currentPlayerChanger.CurrentPlayer.IsPlayerTurn)
			{
				_figureMover.Move(Input.mousePosition);
			}
		}
		
		private void DisableButtons(GameResult gameResult)
		{
			_updater.Disattach(this);
			_currentPlayerChanger.OnPlayerChanged -= RefreshGenerateButton;
			_endGameObserver.OnGameEnded -= DisableButtons;
			MutableView.GenerateButton.gameObject.SetActive(false);
			MutableView.ConfirmButton.gameObject.SetActive(false);
			MutableView.ReadyButton.gameObject.SetActive(false);
		}

		private void OnGenerateButtonClick()
		{
			_doTurnMover.StartTurn();

			if (_currentPlayerChanger.CurrentPlayer.IsFirstTurn == false)
			{
				MutableView.GenerateButton.gameObject.SetActive(false);
				MutableView.ConfirmButton.gameObject.SetActive(true);
			}
		}

		private void OnConfirmButtonClick()
		{
			if (_doTurnMover.TryConfirmFigure())
			{
				MutableView.ConfirmButton.gameObject.SetActive(false);

				_playerActions.ChangePlayer();
			}
		}

		private void OnReadyButtonClick()
		{
			_playerActions.PlayerReady();
			MutableView.ReadyButton.gameObject.SetActive(false);
		}

		private void RefreshGenerateButton()
		{
			MutableView.GenerateButton.gameObject.SetActive(_currentPlayerChanger.LocalPlayer.IsPlayerTurn);
			MutableView.ConfirmButton.gameObject.SetActive(false);
		}
	}
}