﻿using Code.Framework;
using UnityEngine;

namespace Code.Game
{
	public class LocalData : IInitializable
	{
		public float SfxVolume => _sfxVolume;
		public float MusicVolume => _musicVolume;
		
		private const string SfxVolumeKey = "SfxVolume";
		private const string MusicVolumeKey = "MusicVolume";
		private const float DefaultSfxVolume = .3f;
		private const float DefaultMusicVolume = .3f;

		private float _sfxVolume;
		private float _musicVolume;

		public void Init()
		{
			_sfxVolume = PlayerPrefs.GetFloat(SfxVolumeKey, DefaultSfxVolume);
			_musicVolume = PlayerPrefs.GetFloat(MusicVolumeKey, DefaultMusicVolume);
		}

		public void SaveSfxVolume(float value)
		{
			PlayerPrefs.SetFloat(SfxVolumeKey, value);
		}
		
		public void SaveMusicVolume(float value)
		{
			PlayerPrefs.SetFloat(MusicVolumeKey, value);
		}
	}
}