﻿using System;
using Code.Framework;
using Photon.Pun;
using UnityEngine;

namespace Code.Game
{
	public  enum GameResult
	{
		Default,
		Win,
		Defeat,
		Draw
	}
	
	public class EndGameObserver : IInitializable
	{
		[Inject] private readonly GridModel _gridModel;
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly PlayerData _playerData;
		[Inject] private readonly LobbyManager _lobbyManager;

		public event Action<GameResult> OnGameEnded;

		public void Init()
		{
			_gridModel.OnAllGridBusy += EndGameOnAllGridBusy;

			if (PhotonNetwork.IsConnected)
			{
				_lobbyManager.OnPlayerLeftRoomEvent += EndGameOnPlayerLeftRoom;
			}
		}

		private void EndGameOnPlayerLeftRoom()
		{
			EndGame(GameResult.Win);
		}

		private void EndGameOnAllGridBusy()
		{
			EndGame(GetWinner());
		}

		private void EndGame(GameResult gameResult)
		{
			_gridModel.OnAllGridBusy -= EndGameOnAllGridBusy;
			
			if (PhotonNetwork.IsConnected)
			{
				_lobbyManager.OnPlayerLeftRoomEvent -= EndGameOnPlayerLeftRoom;
			}
			
			OnGameEnded?.Invoke(gameResult);
			
			_playerData.RefreshStats(gameResult);
			_lobbyManager.ExitFromRoom();
		}
		
		private GameResult GetWinner()
		{
			var localPlayer = _currentPlayerChanger.LocalPlayer;
			var allScore = _gridModel.Height * _gridModel.Width;
			var halfAllScore = Mathf.FloorToInt(allScore * .5f);
			
			if (localPlayer.Score == halfAllScore)
			{
				return GameResult.Draw;
			}
			else
			{
				return localPlayer.Score > halfAllScore ? GameResult.Win : GameResult.Defeat;
			}
		}
	}
}