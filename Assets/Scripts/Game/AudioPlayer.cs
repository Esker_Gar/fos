﻿using UnityEngine;

namespace Code.Game
{
	public class AudioPlayer : MonoBehaviour
	{
		[SerializeField] private AudioSource _sfxSource;
		[SerializeField] private AudioSource _musicSource;

		public static bool IsInstantiated => _instance != null;
		private static AudioPlayer _instance;

		private void Awake()
		{
			_instance = this;
		}

		public static void PlaySfxShot(AudioClip clip)
		{
			if (clip)
			{
				_instance._sfxSource.PlayOneShot(clip);
			}
		}

		public static void PlayMusic(AudioClip clip)
		{
			if (clip)
			{
				_instance._musicSource.clip = clip;
				_instance._musicSource.Play();
			}
		}

		public static void PauseMusic()
		{
			_instance._musicSource.Pause();
		}

		public static void ResumeMusic()
		{
			_instance._musicSource.Play();
		}
		
		public static void SetSfxVolume(float value)
		{
			_instance._sfxSource.volume = value;
		}

		public static void SetMusicVolume(float value)
		{
			_instance._musicSource.volume = value;
		}
	}
}