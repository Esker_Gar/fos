﻿using Code.Framework;
using TMPro;
using UnityEngine;

namespace Code.Game
{
	public class ChangeNicknameUiView : UiView
	{
		[SerializeField] private TMP_InputField _inputField;
		[SerializeField] private ButtonComponentView _confirmButton;
		[SerializeField] private ButtonComponentView _cancelButton;

		public TMP_InputField InputField => _inputField;
		public ButtonComponentView ConfirmButton => _confirmButton;
		public ButtonComponentView CancelButton => _cancelButton;
	}

	public class ChangeNicknameUiViewController : GenericController<ChangeNicknameUiView>
	{
		[Inject] private readonly PlayerData _playerData;
		
		protected override void HandleInit()
		{
			if (_playerData.TryGetPlayerInfo(out var playerInfoData))
			{
				MutableView.InputField.text = playerInfoData.Name;
			}
			
			var isInputTextEmpty = string.IsNullOrEmpty(MutableView.InputField.text) == false;
			
			MutableView.ConfirmButton.SetOnClick(OnConfirmButtonClicked);
			MutableView.ConfirmButton.SetInteractiveState(isInputTextEmpty);
			MutableView.CancelButton.SetInteractiveState(isInputTextEmpty);
			MutableView.CancelButton.SetOnClick(OnCancelButtonClicked);
			MutableView.InputField.onValueChanged.AddListener((value) =>
			{
				var isInputEmpty = string.IsNullOrEmpty(value) == false;
				
				MutableView.ConfirmButton.SetInteractiveState(isInputEmpty);
				MutableView.CancelButton.SetInteractiveState(isInputEmpty);
			});
		}

		private void OnConfirmButtonClicked()
		{
			_playerData.SetPlayerName(MutableView.InputField.text);
			MutableView.SetActive(false);
		}

		private void OnCancelButtonClicked()
		{
			MutableView.SetActive(false);
		}
	}
}