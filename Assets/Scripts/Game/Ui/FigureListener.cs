﻿using System.Collections.Generic;
using Code.Framework;

namespace Code.Game
{
	public class FigureListener : IInitializable
	{
		[Inject] private readonly FigureAnimator _figureAnimator;
		[Inject] private readonly TurnMover _doTurnMover;
		[Inject] private readonly FigureMover _figureMover;
		[Inject] private readonly GridModel _gridModel;

		public void Init()
		{
			_doTurnMover.OnPlayerCreateFigure += OnCreateFigure;
			_figureMover.OnFigureMoved += OnFigureMoved;
			_gridModel.OnTakenAllSelectedCells += OnTakenAllSelectedCells;
		}

		private void OnCreateFigure(AbstractGrid _)
		{
			_figureAnimator.StopAppearingGeneratedFigureOnGridAnimation(true, FigureAnimation.Add);
			_figureAnimator.AppearingFigureOnGridAnimation(_gridModel.CurrentFigure.Cells, FigureAnimation.Create);
		}

		private void OnFigureMoved()
		{
			_figureAnimator.StopAppearingGeneratedFigureOnGridAnimation(false, FigureAnimation.Create);
		}

		private void OnTakenAllSelectedCells(IEnumerable<Cell> cells)
		{
			_figureAnimator.AppearingFigureOnGridAnimation(cells, FigureAnimation.Add);
		}
	}
}