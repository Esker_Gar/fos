﻿using System;
using Code.Framework;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Code.Game
{
	public class ButtonComponentView : UiView, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
	{
		[SerializeField] private AudioClip _clickSound;
		[SerializeField] private Animator _animator;
		[SerializeField] private Image _buttonImage;
		[SerializeField] private Color _unIntaractiveColor;
		[SerializeField] private bool _isInteractive = true;
		[SerializeField] private bool _isBlinking;
		[SerializeField] private bool _isMuted;

		private int _downParameterHash;
		private int _blinkParameterHash;
		private Action _onClick;

		protected void Awake()
		{
			_downParameterHash = Animator.StringToHash("down");
			_blinkParameterHash = Animator.StringToHash("blink");
		}

		private void OnEnable()
		{
			_animator.SetBool(_blinkParameterHash, _isBlinking);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (_isInteractive)
			{
				_animator.SetBool(_downParameterHash, true);
			}
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			if (_isInteractive)
			{
				_animator.SetBool(_downParameterHash, false);
			}
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.button == PointerEventData.InputButton.Left && _isInteractive)
			{
				HandleClick();
			}
		}

		public void SetInteractiveState(bool state)
		{
			_isInteractive = state;

			_buttonImage.color = _isInteractive == false ? _unIntaractiveColor : Color.white;
		}

		public void SetOnClick(Action action)
		{
			_onClick = action;
		}

		private void HandleClick()
		{
			if (_clickSound != null && _isMuted == false)
			{
				AudioPlayer.PlaySfxShot(_clickSound);
			}
			
			_onClick?.Invoke();
		}
	}
}