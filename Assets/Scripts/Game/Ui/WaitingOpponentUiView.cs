﻿using System;
using Code.Framework;
using UnityEngine;

namespace Code.Game
{
	public class WaitingOpponentUiView : UiView
	{
		[SerializeField] private ButtonComponentView _cancelButton;

		public void SetCancelButtonAction(Action action)
		{
			_cancelButton.SetOnClick(action);
		}
	}
}