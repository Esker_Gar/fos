﻿using System;
using System.Collections.Generic;
using Code.Framework;
using Code.Static;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Game
{
	public class CellGeneratorUiView : UiView
	{
		[SerializeField] private RectTransform _cellsContainer;
		[SerializeField] private GridLayoutGroup _gridLayout;

		public RectTransform CellsContainer => _cellsContainer;
		public GridLayoutGroup GridLayout => _gridLayout;
	}

	public class CellGeneratorUiViewController : GenericController<CellGeneratorUiView>
	{
		[Inject] private readonly CellFactory _cellFactory;
		[Inject] private readonly CellUiView _cellUiViewPrefab;
		[Inject] private readonly TurnMover _turnMover;
		[Inject] private readonly Configs _configs;
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;

		private readonly List<CellUiViewController> _cellControllers = new();
		private Sequence _appearingAnimationSequence;

		protected override void HandleInit()
		{
			MutableView.GridLayout.cellSize = _cellUiViewPrefab.RectTransform.sizeDelta;
			_turnMover.OnPlayerCreateFigure += OnPlayerCreateFigure;
			_turnMover.OnConfirmFigure += OnConfirmFigure;
			_currentPlayerChanger.OnPlayerChanged += DestroyFigure;
		}

		protected override void HandleDestroy()
		{
			_turnMover.OnPlayerCreateFigure -= OnPlayerCreateFigure;
			_turnMover.OnConfirmFigure -= OnConfirmFigure;
			_currentPlayerChanger.OnPlayerChanged -= DestroyFigure;
		}

		private void OnConfirmFigure()
		{
			DestroyFigure();
		}

		private void DestroyFigure()
		{
			_appearingAnimationSequence?.Kill();

			MutableView.CellsContainer.DestroyChildren();
		}

		private void OnPlayerCreateFigure(AbstractGrid figure)
		{
			DestroyFigure();
			_cellControllers.Clear();
			SetGrid(figure.Height);
			figure.PassGrid(CreateCells);
			_appearingAnimationSequence.Kill();
			_appearingAnimationSequence = DOTween.Sequence();
			var configAnimations = _configs.ConfigAnimations;
			var durationForOneCell = configAnimations.CellsPunchScaleAnimationDuration / _cellControllers.Count;

			foreach (var cellUiViewController in _cellControllers)
			{
				_appearingAnimationSequence.Append(cellUiViewController.GetPunchScaleAnimation(configAnimations.CellsPunchScaleAnimationValue, durationForOneCell));
			}

			_appearingAnimationSequence.Play();
		}

		private void SetGrid(int height)
		{
			MutableView.GridLayout.constraintCount = height;
		}
		
		private void CreateCells(Cell cell)
		{
			var (cellUiView, cellUiViewController) = _cellFactory.Create(MutableView.CellsContainer, cell);
			cellUiView.SetActive(false);
			
			_cellControllers.Add(cellUiViewController);
		}
	}
}