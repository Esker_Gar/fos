﻿using Code.Framework;
using Code.Static;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Code.Game
{
	public class MenuHudUiView : UiView
	{
		[SerializeField] private ButtonComponentView _startGameButton;
		[SerializeField] private ButtonComponentView _trainingButton;
		[SerializeField] private ButtonComponentView _gameRulesButton;
		[SerializeField] private ButtonComponentView _changeNicknameButton;
		[SerializeField] private TMP_Text _connectionText;
		[SerializeField] private TMP_Text _connectedRegion;
		[SerializeField] private Slider _sfxSlider;
		[SerializeField] private Slider _musicSlider;
		[SerializeField] private TMP_Text _nicknameText;
		[SerializeField] private NoteComponentView _noteComponentView;
		[SerializeField] private ChangeNicknameUiView _changeNicknameUiView;
		[SerializeField] private WaitingOpponentUiView _waitingOpponent;

		public ButtonComponentView StartGameButton => _startGameButton;
		public ButtonComponentView TrainingButton => _trainingButton;
		public ButtonComponentView GameRulesButton => _gameRulesButton;
		public ButtonComponentView ChangeNicknameButton => _changeNicknameButton;
		public NoteComponentView NoteComponentView => _noteComponentView;
		public TMP_Text ConnectionText => _connectionText;
		public TMP_Text ConnectedRegion => _connectedRegion;
		public Slider SfxSlider => _sfxSlider;
		public Slider MusicSlider => _musicSlider;
		public TMP_Text NicknameText => _nicknameText;
		public ChangeNicknameUiView ChangeNicknameUiView => _changeNicknameUiView;
		public WaitingOpponentUiView WaitingOpponent => _waitingOpponent;
	}

	public class MenuHudUiViewController : GenericController<MenuHudUiView>
	{
		[Inject] private readonly LobbyManager _lobbyManager;
		[Inject] private readonly Configs _configs;
		[Inject] private readonly LocalData _localData;
		[Inject] private readonly PlayerData _playerData;
		[Inject] private readonly ISceneLoader _sceneLoader;
		[Inject] private readonly Factory<ChangeNicknameUiViewController> _changeNicknameUiViewControllerFactory;

		protected override void HandleInit()
		{
			var changeNicknameUiViewController = _changeNicknameUiViewControllerFactory.Create();
			changeNicknameUiViewController.SetView(MutableView.ChangeNicknameUiView);
			changeNicknameUiViewController.Init();

			MutableView.NoteComponentView.SetActive(false);
			MutableView.ChangeNicknameUiView.SetActive(false);
			MutableView.StartGameButton.SetInteractiveState(false);

			MutableView.StartGameButton.SetOnClick(OnStartGameButtonClicked);
			MutableView.TrainingButton.SetOnClick(OnTrainingButtonClicked);
			MutableView.GameRulesButton.SetOnClick(OnGameRulesButtonClicked);
			MutableView.ChangeNicknameButton.SetOnClick(OnChangeNicknameButtonClicked);
			MutableView.NoteComponentView.SetButtonClick(OnNoteButtonClicked);

			MutableView.ConnectionText.color = _configs.ConfigColors.WaitConnectionColor;
			MutableView.ConnectionText.SetText(_configs.ConfigPhrases.Connecting);
			
			MutableView.WaitingOpponent.SetCancelButtonAction(() =>
			{
				_lobbyManager.ExitFromRoom();
				MutableView.WaitingOpponent.SetActive(false);
			});
			MutableView.WaitingOpponent.SetActive(false);

			_lobbyManager.OnConnectedToMasterEvent += OnConnectedToMaster;

			_lobbyManager.OnJoinedRoomEvent += OnJoinedRoom;

			if (_playerData.TryGetPlayerInfo(out var playerInfoData))
			{
				MutableView.NicknameText.SetText(playerInfoData.Name);
			}
			else
			{
				OpenGameRules();
				MutableView.ChangeNicknameUiView.SetActive(true);
			}

			AudioPlayer.SetMusicVolume(_localData.MusicVolume);
			AudioPlayer.SetSfxVolume(_localData.SfxVolume);
			MutableView.SfxSlider.value = _localData.SfxVolume;
			MutableView.MusicSlider.value = _localData.MusicVolume;

			MutableView.SfxSlider.onValueChanged.AddListener((value) =>
			{
				AudioPlayer.SetSfxVolume(value);
				_localData.SaveSfxVolume(value);
			});

			MutableView.MusicSlider.onValueChanged.AddListener((value) =>
			{
				AudioPlayer.SetMusicVolume(value);
				_localData.SaveMusicVolume(value);
			});

			_playerData.OnPlayerNameChanged += OnPlayerDataChanged;
			
			AudioPlayer.PlayMusic(_configs.ConfigAudio.MenuBackground);
		}

		protected override void HandleDestroy()
		{
			_playerData.OnPlayerNameChanged -= OnPlayerDataChanged;
			_lobbyManager.OnConnectedToMasterEvent -= OnConnectedToMaster;
			_lobbyManager.OnJoinedRoomEvent -= OnJoinedRoom;
		}

		private void OnConnectedToMaster(string region)
		{
			MutableView.ConnectionText.SetText(_configs.ConfigPhrases.Connected);
			MutableView.ConnectionText.color = _configs.ConfigColors.ConnectedColor;
			MutableView.ConnectedRegion.SetText(region.ToUpper());
			MutableView.StartGameButton.SetInteractiveState(true);
		}

		private void OnJoinedRoom()
		{
			MutableView.WaitingOpponent.SetActive(true);
		}

		private void OnPlayerDataChanged(PlayerInfoData playerInfoData)
		{
			MutableView.NicknameText.SetText(playerInfoData.Name);
		}

		private void OnStartGameButtonClicked()
		{
			_lobbyManager.CreateOrJoinRoom();
		}

		private void OnTrainingButtonClicked()
		{
			_sceneLoader.LoadScene(new BattleScene(), LoadSceneMode.Single);
		}

		private void OnGameRulesButtonClicked()
		{
			OpenGameRules();
		}

		private void OpenGameRules()
		{
			MutableView.NoteComponentView.SetNoteText(_configs.ConfigPhrases.GameRules);
			MutableView.NoteComponentView.SetActive(true);
		}

		private void OnChangeNicknameButtonClicked()
		{
			MutableView.ChangeNicknameUiView.SetActive(true);
		}

		private void OnNoteButtonClicked()
		{
			MutableView.NoteComponentView.SetActive(false);
		}
	}
}