﻿using System;
using Code.Framework;
using TMPro;
using UnityEngine;

namespace Code.Game
{
	public class NoteComponentView : UiView
	{
		[SerializeField] private TMP_Text _noteText;
		[SerializeField] private ButtonComponentView _button;

		public void SetNoteText(string noteText)
		{
			_noteText.SetText(noteText);
		}

		public void SetButtonClick(Action action)
		{
			_button.SetOnClick(action);
		}
	}
}