﻿using Code.Framework;
using TMPro;
using UnityEngine;

namespace Code.Game
{
	public class PlayerInfoUiView : UiView
	{
		[SerializeField] private TMP_Text _nameText;
		[SerializeField] private TMP_Text _victoryText;
		[SerializeField] private TMP_Text _drawText;
		[SerializeField] private TMP_Text _defeatText;

		public TMP_Text NameText => _nameText;
		public TMP_Text VictoryText => _victoryText;
		public TMP_Text DrawText => _drawText;
		public TMP_Text DefeatText => _defeatText;
	}

	public class PlayerInfoUiViewController : GenericController<PlayerInfoUiView>
	{
		public void SetPlayerInfo(PlayerInfoData playerInfoData)
		{
			MutableView.NameText.SetText(playerInfoData.Name);
			MutableView.VictoryText.SetText(playerInfoData.Victory.ToString());
			MutableView.DrawText.SetText(playerInfoData.Draw.ToString());
			MutableView.DefeatText.SetText(playerInfoData.Defeat.ToString());
		}
	}
}

