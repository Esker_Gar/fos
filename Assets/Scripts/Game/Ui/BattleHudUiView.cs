﻿using Code.Framework;
using Code.Static;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Code.Game
{
	public class BattleHudUiView : UiView
	{
		[SerializeField] private PlayerInfoUiView _firstPlayerInfoUiView;
		[SerializeField] private PlayerInfoUiView _secondPlayerInfoUiView;
		[SerializeField] private TMP_Text _statusText;
		[SerializeField] private NoteComponentView _resultNoteUiView;
		[SerializeField] private TMP_Text _remainingTime;

		public PlayerInfoUiView FirstPlayerInfoUiView => _firstPlayerInfoUiView;
		public PlayerInfoUiView SecondPlayerInfoUiView => _secondPlayerInfoUiView;
		public TMP_Text StatusText => _statusText;
		public NoteComponentView ResultNoteUiView => _resultNoteUiView;
		public TMP_Text RemainingTime => _remainingTime;
	}

	public class BattleHudUiViewController : GenericController<BattleHudUiView>, IUpdatable
	{
		[Inject] private readonly PlayerData _playerData;
		[Inject] private readonly IPlayerActions _playerActions;
		[Inject] private readonly IPlayersHolder _playersHolder;
		[Inject] private readonly Factory<PlayerInfoUiViewController> _playerInfoUiViewControllerFactory;
		[Inject] private readonly IMessenger _messenger;
		[Inject] private readonly EndGameObserver _endGameObserver;
		[Inject] private readonly Configs _configs;
		[Inject] private readonly AdsManager _adsManager;
		[Inject] private readonly ISceneLoader _sceneLoader;
		[Inject] private readonly IUpdater _updater;
		[Inject] private readonly TurnController _turnController;
		
		private PlayerInfoUiView _oppositePlayerInfoUiView;
		private PlayerInfoData _localPlayerInfoData;
		private PlayerInfoData _oppositePlayerInfoData;

		protected override void HandleInit()
		{
			MutableView.ResultNoteUiView.SetActive(false);

			var localPlayerInfoUiView = default(PlayerInfoUiView);
				
			if (_playersHolder.GetLocalPlayer().Direction == Direction.DownRight)
			{
				localPlayerInfoUiView = MutableView.FirstPlayerInfoUiView;
				_oppositePlayerInfoUiView = MutableView.SecondPlayerInfoUiView;
			}
			else
			{
				localPlayerInfoUiView = MutableView.SecondPlayerInfoUiView;
				_oppositePlayerInfoUiView = MutableView.FirstPlayerInfoUiView;
			}
			
			if (_playerData.TryGetPlayerInfo(out _localPlayerInfoData))
			{
				var firstPlayerInfoController = _playerInfoUiViewControllerFactory.Create();
				firstPlayerInfoController.SetView(localPlayerInfoUiView);
				firstPlayerInfoController.Init();
				firstPlayerInfoController.SetPlayerInfo(_localPlayerInfoData);
			}
			
			_playerActions.OnInitPlayerInfoData += InitSecondPlayerInfo;
			_playerActions.InitOppositePlayerInfoData();

			_messenger.OnMessage += RefreshStatus;
			_endGameObserver.OnGameEnded += ShowResultWindow;
			
			MutableView.ResultNoteUiView.SetButtonClick(OnNoteButtonClicked);
			
			AudioPlayer.PlayMusic(_configs.ConfigAudio.GameBackground);
			
			_updater.Attach(this);
		}

		protected override void HandleDestroy()
		{
			_playerActions.OnInitPlayerInfoData -= InitSecondPlayerInfo;
			_messenger.OnMessage -= RefreshStatus;
			_endGameObserver.OnGameEnded -= ShowResultWindow;
			_updater.Disattach(this);
		}
		
		public void Update(float dt)
		{
			MutableView.RemainingTime.SetText(_turnController.CurrentTurnTime.ToString("00"));
		}

		private void InitSecondPlayerInfo(PlayerInfoData playerInfoData)
		{
			_oppositePlayerInfoData = playerInfoData;
			var secondPlayerInfoController = _playerInfoUiViewControllerFactory.Create();
			secondPlayerInfoController.SetView(_oppositePlayerInfoUiView);
			secondPlayerInfoController.Init();
			secondPlayerInfoController.SetPlayerInfo(_oppositePlayerInfoData);
		}

		private void RefreshStatus(string message)
		{
			MutableView.StatusText.SetText(message);
		}

		private void ShowResultWindow(GameResult gameResult)
		{
			MutableView.ResultNoteUiView.SetActive(true);
			MutableView.StatusText.gameObject.SetActive(false);

			var configPhrases = _configs.ConfigPhrases;
			var resultNote = configPhrases.WinNote;

			switch (gameResult)
			{
				case GameResult.Win:
					resultNote = resultNote.Replace("{{player}}", _localPlayerInfoData.Name);
					break;
				case GameResult.Defeat:
					resultNote = resultNote.Replace("{{player}}", _oppositePlayerInfoData.Name);
					break;
				case GameResult.Draw:
					resultNote = configPhrases.DrawNote;
					break;
			}
			
			MutableView.ResultNoteUiView.SetNoteText(resultNote);
		}

		private void OnNoteButtonClicked()
		{
			_adsManager.OnAdsCompleted += () => _sceneLoader.LoadScene(new MenuScene(), LoadSceneMode.Single);
			_adsManager.ShowAds();
		}
	}
}