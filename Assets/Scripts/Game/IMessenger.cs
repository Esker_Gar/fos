﻿using System;
using Code.Framework;

namespace Code.Game
{
	public interface IMessenger : IInitializable
	{
		event Action<string> OnMessage;
	}
}