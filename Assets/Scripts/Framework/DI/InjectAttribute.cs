﻿using System;

namespace Code.Framework
{
	[AttributeUsage(AttributeTargets.Field)]
	public sealed class InjectAttribute : Attribute
	{
	}
}