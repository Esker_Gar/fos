using System;
using System.Collections.Generic;

namespace Code.Framework
{
	public class DiContainer
	{
		public IReadOnlyDictionary<Type, object> ObjectByType => _objectByType;
		public IReadOnlyDictionary<Type, Type> TypeByInterface => _typeByInterface;

		private readonly Dictionary<Type, object> _objectByType = new Dictionary<Type, object>();
		private readonly Dictionary<Type, Type> _typeByInterface = new Dictionary<Type, Type>();

		public void AddObjectByType(object instance)
		{
			var type = instance.GetType();

			if (_objectByType.ContainsKey(type) == false)
			{
				_objectByType.Add(type, instance);
			}
		}

		public void AddObjectByType<TInterface>(object instance)
		{
			var type = instance.GetType();
			var @interface = type.GetInterface(typeof(TInterface).Name);

			if (_typeByInterface.ContainsKey(@interface) == false)
			{
				_typeByInterface.Add(@interface, type);
			}

			if (_objectByType.ContainsKey(@interface) == false)
			{
				_objectByType.Add(@interface, instance);
			}
		}
	}
}