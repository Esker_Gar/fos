﻿using System;
using UnityEngine;

namespace Code.Framework
{
	public class ContextProvider
	{
		private readonly DiContainer _container;
		private readonly DiContainer _parentContainer;

		public ContextProvider(DiContainer container, DiContainer parentContainer = null)
		{
			_container = container;
			_parentContainer = parentContainer;
		}

		public void FillContext()
		{
			foreach (var pair in _container.ObjectByType)
			{
				var type = pair.Key;
				var instance = pair.Value;

				if (type.IsInterface)
				{
					if (_container.TypeByInterface.TryGetValue(type, out var value) || (_parentContainer != null && _parentContainer.TypeByInterface.TryGetValue(type, out value)))
					{
						type = value;
					}
					else
					{
						Debug.LogError($"{type} was not presented on interface dictionary");
					}
				}

				FillInjectFields(type, instance);
			}
		}

		public void FillInjectFields(Type type, object instance)
		{
			var typeAnalyzer = new TypeAnalyzer();

			var injectFields = typeAnalyzer.GetInjectFields(type);

			foreach (var injectField in injectFields)
			{
				if (_container.ObjectByType.TryGetValue(injectField.FieldType, out var injectInstance) ||
				    (_parentContainer != null && _parentContainer.ObjectByType.TryGetValue(injectField.FieldType, out injectInstance)))
				{
					injectField.SetValue(instance, injectInstance);
				}
				else
				{
					Debug.LogError($"{injectField.FieldType} was not presented on dictionary");
				}
			}
		}
	}
}