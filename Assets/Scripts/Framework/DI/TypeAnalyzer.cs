﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Code.Framework
{
	public class TypeAnalyzer
	{
		public List<FieldInfo> GetInjectFields(Type type)
		{
			var returnValue = new List<FieldInfo>();

			var fields = type.GetFields(
				BindingFlags.NonPublic
				| BindingFlags.Instance
				| BindingFlags.DeclaredOnly);

			foreach (var fieldInfo in fields)
			{
				if (CheckInjectAttribute(type, fieldInfo))
				{
					returnValue.Add(fieldInfo);
				}
			}

			return returnValue;
		}

		private bool CheckInjectAttribute(Type type, FieldInfo fieldInfo)
		{
			if (Attribute.IsDefined(fieldInfo, typeof(InjectAttribute)))
			{
				var isReadOnly = fieldInfo.IsInitOnly;

				if (isReadOnly == false)
				{
					throw new Exception($"[TypeAnalyzer::GetFieldsInfo] Exception while injecting into \"{type}\". Injected field must be \"readonly\". ");
				}

				return true;
			}

			return false;
		}
	}
}