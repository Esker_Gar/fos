﻿using UnityEngine;

namespace Code.Framework
{
	public class InitContext : MonoBehaviour, IInitializable
	{
		protected virtual void HandleInit()
		{}

		public void Init()
		{
			HandleInit();
		}
	}
}