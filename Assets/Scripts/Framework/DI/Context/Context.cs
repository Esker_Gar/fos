﻿using UnityEngine;

namespace Code.Framework
{
	public class Context : MonoBehaviour, IInitializable<Context.InitData>
	{
		public readonly struct InitData
		{
			public readonly InitContext InitContext;

			public InitData(InitContext initContext)
			{
				InitContext = initContext;
			}
		}

		public Context Parent => _parent;

		private Context _parent;
		private DiContainer _container;
		private ContextProvider _contextProvider;
		private Injector _injector;
		private Binder _binder;
		private InitContext _initContext;

		protected virtual void HandleInit(Binder binder)
		{}

		public void Init(InitData initData)
		{
			_container = new DiContainer();
			_injector = new Injector(_container);
			_binder = new Binder(_injector);

			_injector.Inject(this);
			
			_initContext = initData.InitContext;
			_injector.Inject(_initContext);
			
			HandleInit(_binder);
		}

		public void FillContext()
		{
			_contextProvider = new ContextProvider(_container, _parent != null ? _parent._container : null);
			_injector.Inject(_contextProvider);

			_contextProvider.FillContext();
		}

		public void SetParent(Context context)
		{
			_parent = context;
		}
	}
}