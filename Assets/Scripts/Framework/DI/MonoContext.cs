﻿using UnityEngine;

namespace Code.Framework
{
	public class MonoContext : MonoBehaviour
	{
		[SerializeField] private Context _contextHandler;
		[SerializeField] private InitContext _initContext;

		private void Awake()
		{
			_contextHandler.Init(new Context.InitData(_initContext));
		}

		private void Start()
		{
			_contextHandler.FillContext();
			_initContext.Init();
		}
	}
}