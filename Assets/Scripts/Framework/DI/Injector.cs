﻿namespace Code.Framework
{
	public class Injector
	{
		private readonly DiContainer _container;

		public Injector(DiContainer container)
		{
			_container = container;
		}

		public void Inject(object instance)
		{
			_container.AddObjectByType(instance);
		}

		public void Inject<TInterface>(object instance)
		{
			_container.AddObjectByType<TInterface>(instance);
		}
	}
}