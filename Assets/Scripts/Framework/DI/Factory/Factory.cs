﻿
namespace Code.Framework
{
	public class Factory<T>
	where T : class, new()
	{
		[Inject] private readonly ContextProvider _contextProvider;

		public T Create()
		{
			var instance = new T();

			_contextProvider.FillInjectFields(typeof(T), instance);

			return instance;
		}
	}
}