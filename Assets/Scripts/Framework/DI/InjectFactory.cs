﻿namespace Code.Framework
{
	public class InjectFactory<T>
		where T : class, new()
	{
		public T CreateInstance()
		{
			var instance = new T();

			return instance;
		}
	}
}