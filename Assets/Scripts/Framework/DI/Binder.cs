﻿using Code.Game;

namespace Code.Framework
{
	public class Binder
	{
		private readonly Injector _injector;

		public Binder(Injector injector)
		{
			_injector = injector;
		}

		public void AsSerializedField(object instance)
		{
			_injector.Inject(instance);
		}
		
		public void AsSerializedField<TInterface>(object instance)
		{
			_injector.Inject<TInterface>(instance);
		}

		public void AsInjectFactory<T>()
			where T : class, new()
		{
			AsInstance<Factory<T>>();
		}

		public void AsInstance<T>()
		where T : class, new()
		{
			var injectFactory = new InjectFactory<T>();
			var instance = injectFactory.CreateInstance();

			_injector.Inject(instance);
		}

		public void AsInterface<T, TInterface>()
		where T : class, TInterface, new()
		{
			var injectFactory = new InjectFactory<T>();
			var instance = injectFactory.CreateInstance();

			_injector.Inject<TInterface>(instance);
		}
	}
}