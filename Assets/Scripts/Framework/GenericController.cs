﻿namespace Code.Framework
{
	public abstract class GenericController<T>
		where T : IView
	{
		protected T MutableView {get; private set;}

		protected virtual void HandleInit() {}
		protected virtual void HandleDestroy() {}

		public void SetView(T view)
		{
			MutableView = view;
			MutableView.OnDestroyed += Destroy;
		}

		public void Init()
		{
			HandleInit();
		}

		private void Destroy()
		{
			HandleDestroy();
			MutableView.OnDestroyed -= Destroy;
		}
	}

	public abstract class GenericController<T, TInitData> : GenericController<T>
		where T : IView
		where TInitData : struct
	{
		protected abstract void HandleInit(TInitData initData);

		public void Init(TInitData initData)
		{
			HandleInit(initData);
		}
	}
}