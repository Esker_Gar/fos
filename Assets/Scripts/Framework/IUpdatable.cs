﻿namespace Code.Framework
{
	public interface IUpdatable
	{
		void Update(float dt);
	}
}