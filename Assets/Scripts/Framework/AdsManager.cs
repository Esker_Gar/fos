﻿using System;
using Code.Game;
using Code.Static;

namespace Code.Framework
{
	public class AdsManager : IInitializable
	{
		[Inject] private readonly Configs _configs;

		public event Action OnAdsCompleted;
		
		private bool _isAdsReady;
		
		public void Init()
		{
			IronSource.Agent.init(_configs.ConfigGame.GameId);
			IronSourceEvents.onSdkInitializationCompletedEvent += InitializationComplete;
		}

		public void LoadAds()
		{
			_isAdsReady = false;
			IronSource.Agent.loadInterstitial();
		}

		public void ShowAds()
		{
			if (_isAdsReady)
			{
				AudioPlayer.PauseMusic();
				IronSource.Agent.showInterstitial();
			}
		}

		private void InitializationComplete()
		{
			IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
			IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;        
			IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent; 
			IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent; 
			IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
			IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
			IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
		}

		private void InterstitialAdClosedEvent()
		{
			
		}

		private void InterstitialAdOpenedEvent()
		{
			
		}

		private void InterstitialAdClickedEvent()
		{
			
		}

		private void InterstitialAdShowFailedEvent(IronSourceError obj)
		{
			
		}

		private void InterstitialAdShowSucceededEvent()
		{
			AudioPlayer.ResumeMusic();
			OnAdsCompleted?.Invoke();
			OnAdsCompleted = null;
		}

		private void InterstitialAdLoadFailedEvent(IronSourceError obj)
		{
			
		}

		private void InterstitialAdReadyEvent()
		{
			_isAdsReady = true;
		}
	}
}