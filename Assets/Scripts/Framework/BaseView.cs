﻿using System;
using UnityEngine;

namespace Code.Framework
{
	public class BaseView : MonoBehaviour, IView
	{
		public event Action OnDestroyed;

		protected virtual void HandleAwake(){}
		
		public void SetActive(bool active)
		{
			gameObject.SetActive(active);
		}

		private void Awake()
		{
			HandleAwake();
		}

		private void OnDestroy()
		{
			OnDestroyed?.Invoke();
		}
	}
}