﻿namespace Code.Framework
{
	public interface IUpdater
	{
		void Attach(IUpdatable updatable);
		void Disattach(IUpdatable updatable);
	}
}