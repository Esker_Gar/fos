﻿using UnityEngine;

namespace Code.Framework
{
	public static class ObjectExtension
	{
		public static T Instantiate<T>(this T obj, Transform parent)
			where T : Object
		{
			return Object.Instantiate(obj, parent);
		}

		public static void DestroyChildren(this Transform transform)
		{
			var childCount = transform.childCount;

			for (int i = 0; i < childCount; i++)
			{
				var child = transform.GetChild(i);

				Object.Destroy(child.gameObject);
			}
		}
	}
}