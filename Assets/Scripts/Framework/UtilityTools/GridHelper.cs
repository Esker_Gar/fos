﻿using Code.Static;
using UnityEditor;
using UnityEngine;

namespace Code.Framework
{
	public class GridHelper : MonoBehaviour
	{
		[SerializeField] private RectTransform _cellPrefab;
		[SerializeField] private RectTransform _gridUiViewRect;
		[SerializeField] private RectTransform _cellGeneratorRect;
		[SerializeField] private GridData _gridData;
		[SerializeField] private bool _isDrawGizmo;

		private readonly Vector3 _6x6size = new Vector2(300,300);

		private void Awake()
		{
			if (_gridData.Width * _gridData.Height * .5f % 2 != 0)
			{
				Debug.LogError("Square no even");
			}
		}

#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			if (_isDrawGizmo == false)
			{
				return;
			}

			var square = _gridData.Width * _gridData.Height;
			var cellSize = _cellPrefab.sizeDelta;
			var gridPosition = _gridUiViewRect.position;

			_gridUiViewRect.sizeDelta = new Vector2(_gridData.Width * cellSize.x, _gridData.Height * cellSize.y);
			Gizmos.color = Color.blue;

			Gizmos.DrawWireCube(gridPosition, _gridUiViewRect.sizeDelta);
			Handles.Label(gridPosition, square.ToString());
			
			Gizmos.color = Color.red;
			Gizmos.DrawWireCube(_cellGeneratorRect.position, _6x6size);
		}
#endif
	}
}