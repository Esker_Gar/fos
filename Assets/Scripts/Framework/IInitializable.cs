﻿namespace Code.Framework
{
	public interface IInitializable<in TInitData>
	{
		void Init(TInitData initData);
	}

	public interface IInitializable
	{
		void Init();
	}
}