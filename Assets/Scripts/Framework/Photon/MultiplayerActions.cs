﻿using System;
using System.Collections;
using Code.Game;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Code.Framework
{
	public enum RiseEvents
	{
		Default = -1,
		PlayerReady = 10,
		TakeAllSelectedCells = 11,
		ChangePlayer = 12,
		InitOppositePlayerInfoData = 13
	}

	public class MultiplayerActions : MonoBehaviourPunCallbacks, IPunObservable, IPlayerActions, IOnEventCallback
	{
		[Inject] private readonly GridModel _gridModel;
		[Inject] private readonly CurrentPlayerChanger _currentPlayerChanger;
		[Inject] private readonly PlayerData _playerData;

		public Action OnPlayersReady {get; set;}
		public Action<PlayerInfoData> OnInitPlayerInfoData {get; set;}

		private readonly bool[] _isPlayersReady = new bool[2];
		private bool _isMasterClientDoFirstTurn;
		private readonly RaiseEventOptions _raiseEventOptions = new() {Receivers = ReceiverGroup.Others};

		public void Init()
		{
			PhotonPeer.RegisterType(typeof(Cell), (byte) 'C', Serializable.CellSerialize, Serializable.CellDeserialize);
			PhotonPeer.RegisterType(typeof(Color), (byte) 'A', Serializable.ColorSerialize, Serializable.ColorDeserialize);
			PhotonPeer.RegisterType(typeof(PlayerInfoData), (byte) 'I', Serializable.PlayerInfoDataSerialize, Serializable.PlayerInfoDataDeserialize);
			
			PhotonNetwork.AddCallbackTarget(this);
		}
		
		public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
		{
			if (stream.IsWriting)
			{
				stream.SendNext(_isMasterClientDoFirstTurn);
				
			}
			else if (stream.IsReading)
			{
				_isMasterClientDoFirstTurn = (bool) stream.ReceiveNext();
			}
		}
		
		public void OnEvent(EventData photonEvent)
		{
			if (Enum.TryParse<RiseEvents>(((int)photonEvent.Code).ToString(), out var riseEvent) == false)
			{
				return;
			}

			switch (riseEvent)
			{
				case RiseEvents.PlayerReady:
					var playerNumber = (int) photonEvent.CustomData;
					PlayerReadyHandle(playerNumber);
					break;
				case RiseEvents.TakeAllSelectedCells:
					var content = (object[])photonEvent.CustomData;
					var cells = (Cell[])content[0];
					var color = (Color)content[1];
					TakeAllSelectedCellsHandle(cells, color);
					break;
				case RiseEvents.ChangePlayer:
					ChangePlayerHandle();
					break;
				case RiseEvents.InitOppositePlayerInfoData:
					var playerInfoData = (PlayerInfoData) photonEvent.CustomData;
					InitOppositePlayerInfoDataHandle(playerInfoData);
					break;
			}
		}
		
		public void DoFirstTurn(Action action)
		{
			if (PhotonNetwork.IsMasterClient)
			{
				action?.Invoke();
			}
			else
			{
				StartCoroutine(DeferredTurn(action));
			}
		}

		public void TakeAllSelectedCells(Cell[] cells)
		{
			var content = new object[]
			{
				cells,
				_currentPlayerChanger.CurrentPlayer.Color
			};
			
			PhotonNetwork.RaiseEvent((byte)RiseEvents.TakeAllSelectedCells, content, _raiseEventOptions, SendOptions.SendReliable);

			TakeAllSelectedCellsHandle(cells, _currentPlayerChanger.CurrentPlayer.Color);
		}

		public void ChangePlayer()
		{
			PhotonNetwork.RaiseEvent((byte)RiseEvents.ChangePlayer, null, _raiseEventOptions, SendOptions.SendReliable);

			ChangePlayerHandle();
		}

		public void CreateGridFigure(Cell initCell, int iterationStep, int figureWidth, int figureHeight)
		{
			_gridModel.CreateFigure(initCell, iterationStep, figureWidth, figureHeight);
		}

		public void PlayerReady()
		{
			var playerNumber = PhotonNetwork.LocalPlayer.ActorNumber - 1;
			PhotonNetwork.RaiseEvent((byte)RiseEvents.PlayerReady, playerNumber, _raiseEventOptions, SendOptions.SendReliable);

			PlayerReadyHandle(playerNumber);
		}

		public void InitOppositePlayerInfoData()
		{
			_playerData.TryGetPlayerInfo(out var playerInfoData);
			PhotonNetwork.RaiseEvent((byte)RiseEvents.InitOppositePlayerInfoData, playerInfoData, _raiseEventOptions, SendOptions.SendReliable);
			_playerData.SavePregameLose();
		}
		
		private IEnumerator DeferredTurn(Action action)
		{
			yield return new WaitUntil(() => _isMasterClientDoFirstTurn == false);

			action?.Invoke();
		}
		
		private void PlayerReadyHandle(int playerNumber)
		{
			_isPlayersReady[playerNumber] = true;

			foreach (var isPlayerReady in _isPlayersReady)
			{
				if (isPlayerReady == false)
				{
					return;
				}
			}

			OnPlayersReady?.Invoke();
		}
		
		private void TakeAllSelectedCellsHandle(Cell[] cells, Color playerColor)
		{
			_currentPlayerChanger.CurrentPlayerColor = playerColor;
			_gridModel.TakeAllSelectedCells(cells);
		}

		private void ChangePlayerHandle()
		{
			if (PhotonNetwork.IsMasterClient)
			{
				_isMasterClientDoFirstTurn = true;
			}

			_currentPlayerChanger.ChangePlayer();
		}
		
		private void InitOppositePlayerInfoDataHandle(PlayerInfoData playerInfoData)
		{
			OnInitPlayerInfoData?.Invoke(playerInfoData);
		}
	}
}