﻿using System;
using System.Collections;
using Code.Game;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Player = Photon.Realtime.Player;

namespace Code.Framework
{
	public class LobbyManager : MonoBehaviourPunCallbacks
	{
		[Inject] private readonly ISceneLoader _sceneLoader;

		public event Action<string> OnConnectedToMasterEvent;
		public event Action OnJoinedRoomEvent;
		public event Action OnPlayerLeftRoomEvent;
		
		private Coroutine _playersWaitingCoroutine;

		public void ConnectToServer()
		{
			PhotonNetwork.Disconnect();
			PhotonNetwork.ConnectUsingSettings();
		}

		public override void OnConnectedToMaster()
		{
			OnConnectedToMasterEvent?.Invoke(PhotonNetwork.CloudRegion);
		}

		public override void OnDisconnected(DisconnectCause cause)
		{
			PhotonNetwork.ConnectUsingSettings();
		}

		public void CreateOrJoinRoom()
		{
			PhotonNetwork.JoinRandomOrCreateRoom(
				default,
				default,
				default,
				default,
				default,
				default,
				new RoomOptions
				{
					MaxPlayers = 2
				});
		}

		public override void OnJoinedRoom()
		{
			_playersWaitingCoroutine = StartCoroutine(PlayersWaitingCoroutine());
		}

		public override void OnLeftRoom()
		{
			if (_playersWaitingCoroutine != null)
			{
				StopCoroutine(_playersWaitingCoroutine);
				_playersWaitingCoroutine = null;
			}
		}

		public override void OnPlayerLeftRoom(Player otherPlayer)
		{
			OnPlayerLeftRoomEvent?.Invoke();
		}

		public void ExitFromRoom()
		{
			PhotonNetwork.LeaveRoom();
		}

		private IEnumerator PlayersWaitingCoroutine()
		{
			OnJoinedRoomEvent?.Invoke();
			var playersCountCheck = new WaitWhile(() => PhotonNetwork.CurrentRoom.Players.Count < PhotonNetwork.CurrentRoom.MaxPlayers);

			yield return playersCountCheck;

			_playersWaitingCoroutine = null;

			_sceneLoader.LoadScene(new BattleScene(), LoadSceneMode.Single);
		}
	}
}