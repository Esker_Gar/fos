﻿using System;
using System.Text;
using Code.Game;
using UnityEngine;
using WebSocketSharp;

namespace Code.Framework
{
	public static class Serializable
	{
		public static byte[] CellSerialize(object customType)
		{
			var cell = (Cell)customType;
			var a = Convert.ToByte(cell.HorizontalIndex);
			return new[] { Convert.ToByte(cell.HorizontalIndex), Convert.ToByte(cell.VerticalIndex) };
		}

		public static object CellDeserialize(byte[] data)
		{
			var result = new Cell(data[0], data[1]);
			return result;
		}

		public static byte[] ColorSerialize(object customType)
		{
			var color = (Color)customType;
			var r = BitConverter.GetBytes(color.r);
			var g = BitConverter.GetBytes(color.g);
			var b = BitConverter.GetBytes(color.b);
			var a = BitConverter.GetBytes(color.a);
			
			return new[] {r[0], r[1], r[2], r[3], g[0], g[1], g[2], g[3], b[0], b[1], b[2], b[3], a[0], a[1], a[2], a[3]};
		}

		public static object ColorDeserialize(byte[] data)
		{
			var r = new[]
			{
				data[0],
				data[1],
				data[2],
				data[3]
			};
			
			var g = new[]
			{
				data[4],
				data[5],
				data[6],
				data[7]
			};
			
			var b = new[]
			{
				data[8],
				data[9],
				data[10],
				data[11]
			};
			
			var a = new[]
			{
				data[12],
				data[13],
				data[14],
				data[15]
			};

			var rFloat = BitConverter.ToSingle(r, 0);
			var gFloat = BitConverter.ToSingle(g, 0);
			var bFloat = BitConverter.ToSingle(b, 0);
			var aFloat = BitConverter.ToSingle(a, 0);
			
			var result = new Color(rFloat, gFloat, bFloat, aFloat);
			
			return result;
		}

		public static byte[] PlayerInfoDataSerialize(object customType)
		{
			var playerInfoData = (PlayerInfoData) customType;
			var byteArrayWithoutName = new[] {Convert.ToByte(playerInfoData.Victory), Convert.ToByte(playerInfoData.Draw), Convert.ToByte(playerInfoData.Defeat)};
			var nameByteArray = Encoding.Default.GetBytes(playerInfoData.Name);
			var resultArray = new byte[byteArrayWithoutName.Length + nameByteArray.Length];
			byteArrayWithoutName.CopyTo(resultArray, 0);
			nameByteArray.CopyTo(resultArray, 3);
			return resultArray;
		}

		public static object PlayerInfoDataDeserialize(byte[] data)
		{
			var nameArray = new byte[data.Length - 3];
			
			for (int i = 3; i < data.Length; i++)
			{
				nameArray[i - 3] = data[i];
			}

			var name = Encoding.Default.GetString(nameArray);
			var result = new PlayerInfoData(name,data[0], data[1], data[2]);
			return result;
		}
	}
}