﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Framework
{
	public class Updater : MonoBehaviour, IUpdater
	{
		private readonly List<IUpdatable> _updatables = new List<IUpdatable>();

		private void Update()
		{
			foreach (var updatable in _updatables)
			{
				updatable.Update(Time.deltaTime);
			}
		}

		public void Attach(IUpdatable updatable)
		{
			_updatables.Add(updatable);
		}

		public void Disattach(IUpdatable updatable)
		{
			_updatables.Remove(updatable);
		}
	}
}