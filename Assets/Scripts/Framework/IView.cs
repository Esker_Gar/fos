﻿using System;

namespace Code.Framework
{
	public interface IView
	{
		public event Action OnDestroyed;
	}
}