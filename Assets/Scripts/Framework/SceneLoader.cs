﻿using System;
using Code.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Code.Framework
{
	public interface ISceneLoader : IInitializable
	{
		void LoadScene<T>(T scene, LoadSceneMode loadSceneMode, Context parentContext = null)
			where T : struct, IScene;
	}
	
	public class SceneLoader : ISceneLoader
	{
		private Context _parentContext;
		private Scene _currentScene;
		private Action _sceneLoadedCallback;

		public void Init()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
		}
		
		public void LoadScene<T>(T scene, LoadSceneMode loadSceneMode, Context parentContext = null)
		where T : struct, IScene
		{

			if (parentContext != null)
			{
				_parentContext = parentContext;
			}
			else if (_parentContext == null)
			{
				_parentContext = UnityEngine.Object.FindObjectOfType<SceneContext<T>>().Parent;
			}

			if (loadSceneMode == LoadSceneMode.Single)
			{
				SceneManager.UnloadSceneAsync(_currentScene);
			}

			_sceneLoadedCallback = SceneLoadedCallback<T>;

			SceneManager.LoadScene(scene.GetType().Name, LoadSceneMode.Additive);
		}

		private void SceneLoadedCallback<T>()
		where T : struct, IScene
		{
			
			var context = UnityEngine.Object.FindObjectOfType<SceneContext<T>>();
			context.SetParent(_parentContext);
		}

		private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
		{
			_currentScene = scene;
			
			SceneManager.SetActiveScene(scene);
			
			_sceneLoadedCallback?.Invoke();
		}
	}
}