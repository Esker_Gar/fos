﻿using UnityEngine;

namespace Code.Static
{
	[CreateAssetMenu(fileName = "GridData", menuName = "Data/GridData")]
	public class GridData : ScriptableObject
	{
		[SerializeField] private int _width;
		[SerializeField] private int _height;

		public int Width => _width;
		public int Height => _height;
	}
}