﻿using UnityEngine;

namespace Code.Static
{
	[CreateAssetMenu(fileName = "ConfigColors", menuName = "Configs/Colors")]
	public class ConfigColors : ScriptableObject
	{
		[SerializeField] private Color _waitConnectionColor;
		[SerializeField] private Color _connectedColor;
		[SerializeField] private Color _firstPlayerCellsColor;
		[SerializeField] private Color _secondPlayerCellsColor;
		[SerializeField] private Color _availableCellsColor;
		[SerializeField] private Color _blockedCellsColor;
		[SerializeField] private Color _defaultCellColor;

		public Color WaitConnectionColor => _waitConnectionColor;
		public Color ConnectedColor => _connectedColor;
		public Color FirstPlayerCellsColor => _firstPlayerCellsColor;
		public Color SecondPlayerCellsColor => _secondPlayerCellsColor;
		public Color AvailableCellsColor => _availableCellsColor;
		public Color BlockedCellsColor => _blockedCellsColor;
		public Color DefaultCellColor => _defaultCellColor;
	}
}