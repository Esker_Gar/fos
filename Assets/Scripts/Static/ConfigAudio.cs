﻿using UnityEngine;

namespace Code.Static
{
	[CreateAssetMenu(fileName = "ConfigAudio", menuName = "Configs/Audio")]
	public class ConfigAudio : ScriptableObject
	{
		[SerializeField] private AudioClip _menuBackground;
		[SerializeField] private AudioClip _gameBackground;
		[SerializeField] private AudioClip _createCell;

		public AudioClip MenuBackground => _menuBackground;
		public AudioClip GameBackground => _gameBackground;
		public AudioClip CreateCell => _createCell;
	}
}