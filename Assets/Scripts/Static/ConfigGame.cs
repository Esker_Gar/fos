﻿using UnityEngine;

namespace Code.Static
{
	[CreateAssetMenu(fileName = "ConfigGame", menuName = "Configs/Game")]
	public class ConfigGame : ScriptableObject
	{
		[SerializeField] private string _gameId;
		[SerializeField] private float _turnTime;

		public string GameId => _gameId;
		public float TurnTime => _turnTime;
	}
}