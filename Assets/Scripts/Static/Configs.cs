﻿using UnityEngine;

namespace Code.Static
{
	[CreateAssetMenu(fileName = "Configs", menuName = "Configs/Configs")]
	public class Configs : ScriptableObject
	{
		[SerializeField] private ConfigPhrases _configPhrases;
		[SerializeField] private ConfigColors _configColors;
		[SerializeField] private ConfigAnimations _configAnimations;
		[SerializeField] private ConfigAudio _configAudio;
		[SerializeField] private ConfigGame _configGame;

		public ConfigPhrases ConfigPhrases => _configPhrases;
		public ConfigColors ConfigColors => _configColors;
		public ConfigAnimations ConfigAnimations => _configAnimations;
		public ConfigAudio ConfigAudio => _configAudio;
		public ConfigGame ConfigGame => _configGame;
	}
}