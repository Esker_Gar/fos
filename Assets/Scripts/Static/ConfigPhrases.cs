﻿using UnityEngine;

namespace Code.Static
{
	[CreateAssetMenu(fileName = "ConfigPhrases", menuName = "Configs/Phrases")]
	public class ConfigPhrases : ScriptableObject
	{
		[SerializeField] private string _noteGenerateFigure;
		[SerializeField] private string _noteSpotSelection;
		[SerializeField] private string _noteOpponentTurn;
		[SerializeField] private string _connecting;
		[SerializeField] private string _connected;
		[SerializeField] private string _gameRules;
		[SerializeField] private string _botName;
		[SerializeField] private string _winNote;
		[SerializeField] private string _drawNote;

		public string NoteGenerateFigure => _noteGenerateFigure;
		public string NoteSpotSelection => _noteSpotSelection;
		public string NoteOpponentTurn => _noteOpponentTurn;
		public string Connecting => _connecting;
		public string Connected => _connected;
		public string GameRules => _gameRules;
		public string BotName => _botName;
		public string WinNote => _winNote;
		public string DrawNote => _drawNote;
	}
}