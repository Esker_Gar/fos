﻿using UnityEngine;

namespace Code.Static
{
	[CreateAssetMenu(fileName = "ConfigAnimations", menuName = "Configs/Animations")]
	public class ConfigAnimations : ScriptableObject
	{
		[SerializeField] private float _cellsPunchScaleAnimationDuration;
		[SerializeField] private float _cellsPunchScaleAnimationValue;
		[SerializeField] private float _cellsColorAnimationDuration;

		public float CellsPunchScaleAnimationDuration => _cellsPunchScaleAnimationDuration;
		public float CellsPunchScaleAnimationValue => _cellsPunchScaleAnimationValue;
		public float CellsColorAnimationDuration => _cellsColorAnimationDuration;
	}
}